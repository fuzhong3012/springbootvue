FROM java:8
VOLUME /tmp
ADD springbootvue-1.0.0.jar springbootvue.jar
RUN echo "Asia/Shanghai" > /etc/timezone
RUN cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
EXPOSE 8000
ENTRYPOINT ["java","-jar","/springbootvue.jar"]