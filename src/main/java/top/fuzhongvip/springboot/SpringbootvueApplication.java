package top.fuzhongvip.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("top.fuzhongvip.springboot.mapper")
public class SpringbootvueApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootvueApplication.class, args);
    }

}
