package top.fuzhongvip.springboot.mapper;

import top.fuzhongvip.springboot.entity.FlightJisuData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fuzhong
 * @since 2021-05-31
 */
public interface FlightJisuDataMapper extends BaseMapper<FlightJisuData> {

}
