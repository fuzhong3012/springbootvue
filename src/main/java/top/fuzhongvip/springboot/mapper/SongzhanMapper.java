package top.fuzhongvip.springboot.mapper;

import org.springframework.stereotype.Repository;
import top.fuzhongvip.springboot.entity.Songzhan;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fuzhong
 * @since 2021-04-26
 */
@Repository
public interface SongzhanMapper extends BaseMapper<Songzhan> {

}
