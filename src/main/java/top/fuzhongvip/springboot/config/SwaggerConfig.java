package top.fuzhongvip.springboot.config;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    /**
     * 创建一个Docket对象
     */
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(parameters());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                //标题
                .title("springbootvue系统api文档")
                //简介
                .description("基于springboot和vue.js的前后端分离系统")
                //服务条款
                .termsOfServiceUrl("")
                //作者个人信息
                .contact(new Contact("fuzhong","http://fuzhongvip.top","fuzhong3012@163.com"))
                //版本
                .version("1.0")
                .build();
    }

    private List<Parameter> parameters() {
        Parameter parameter1 = new ParameterBuilder()
             .name("token").description("token令牌")
             .modelRef(new ModelRef("String")).parameterType("header")
             .required(false).build();

        return Lists.newArrayList(parameter1);
	}
}


