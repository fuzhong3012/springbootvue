package top.fuzhongvip.springboot.config;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.jackson.JsonComponent;
import org.springframework.context.annotation.Bean;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

/**
 * @Author : fuzhong
 * @CreateTime : 2020/9/2 16:23
 * @Description : 全局时间格式化
 **/
@JsonComponent
public class DateFormatConfig {

    private String pattern_localDate = "yyyy-MM-dd";
    private String pattern_localDateTime = "yyyy-MM-dd HH:mm:ss";

    /**
     * @author fuzhong
     * @description date 类型全局时间格式化
     * @date 2020/9/2 16:23
     */
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilder() {

        return builder -> {
            TimeZone tz = TimeZone.getTimeZone("Asia/Shanghai");
            DateFormat df = new SimpleDateFormat(pattern_localDateTime);
            df.setTimeZone(tz);
            builder.failOnEmptyBeans(false)
                    .failOnUnknownProperties(false)
                    .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                    .dateFormat(df);
        };
    }

    /**
     * @author fuzhong
     * @description LocalDate 类型全局时间格式化
     * @date 2020/9/2 16:23
     */
    @Bean
    public LocalDateSerializer localDateDeserializer() {
        return new LocalDateSerializer(DateTimeFormatter.ofPattern(pattern_localDate));
    }

    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer1() {
        return builder -> builder.serializerByType(LocalDate.class, localDateDeserializer());
    }

    /**
     * @author fuzhong
     * @description LocalDateTime 类型全局时间格式化
     * @date 2020/9/2 16:23
     */
    @Bean
    public LocalDateTimeSerializer localDateTimeDeserializer() {
        return new LocalDateTimeSerializer(DateTimeFormatter.ofPattern(pattern_localDateTime));
    }

    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {
        return builder -> builder.serializerByType(LocalDateTime.class, localDateTimeDeserializer());
    }
}
