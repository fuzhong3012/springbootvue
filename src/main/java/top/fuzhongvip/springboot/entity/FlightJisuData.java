package top.fuzhongvip.springboot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author fuzhong
 * @since 2021-05-31
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="FlightJisuData对象", description="")
public class FlightJisuData extends Model {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "分组id-用来区分本次")
    private String groupId;

    @ApiModelProperty(value = "请求参数-querydate")
    private String requestQueryDateParams;

    @ApiModelProperty(value = "flightInfoVo-json串")
    private String flightInfoVoJsonParams;

    @ApiModelProperty(value = "SongAndFlightJisuDto-json串")
    private String songAndFlightJisuDtoJsonParams;

    @ApiModelProperty(value = "客户端ip地址")
    private String ipAddr;

    @ApiModelProperty(value = "客户端ip地址info")
    private String ipInfo;

    @ApiModelProperty(value = "极速数据接口调用次数")
    private int invokeCount;

    @ApiModelProperty(value = "极速数据接口调用次数All")
    @TableField(exist = false)
    private int invokeCountAll;

    @ApiModelProperty(value = "删除标识0不删1删除")
    private Integer isDelete;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;


}
