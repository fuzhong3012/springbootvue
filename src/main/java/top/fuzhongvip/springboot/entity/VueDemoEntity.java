package top.fuzhongvip.springboot.entity;

import lombok.Data;

/**
 * @Author : fuzhong
 * @CreateTime : 2020/6/10 13:37
 * @Description :
 **/
@Data
public class VueDemoEntity {

    private String title_chinese;
    private String short_description_chinese;
    private String visible;

}
