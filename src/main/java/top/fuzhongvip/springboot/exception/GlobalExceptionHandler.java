package top.fuzhongvip.springboot.exception;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import top.fuzhongvip.springboot.common.dto.JsonResp;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author : fuzhong
 * @CreateTime : 2020/6/23 10:44
 * @Description : 全局异常处理类
 **/
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    /*@ExceptionHandler(value = CustomException.class)// 一个方法特定处理一种异常
    @ResponseBody
    public ResponseEntity exceptionHandler(CustomException e) {
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("errorTypeName", e.getClass().getName());
        resultMap.put("message", e.getMessage());
        log.error(e.getMessage(), e);
        return ResponseEntity.status(500).body(resultMap);
    }*/

    @ExceptionHandler(value = CustomException.class)// 一个方法特定处理一种异常
    @ResponseBody
    public JsonResp exceptionHandler(CustomException e) {
        return JsonResp.failed(JsonResp.STATUS_FAILURE,  this.setMapToString(e));
    }

    @ExceptionHandler(value = Exception.class)// 一个方法特定处理一种异常
    @ResponseBody
    public JsonResp exceptionHandler(Exception e) {
        return JsonResp.failed(JsonResp.STATUS_ERROR, this.setMapToString(e));
    }

    private String setMapToString(Exception e){
        log.error(e.getMessage(), e);
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("errorTypeName", e.getClass().getName());
        resultMap.put("message", e.getMessage());
        String result = JSON.toJSONString(resultMap);
        return result;
    }
}
