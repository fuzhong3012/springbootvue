package top.fuzhongvip.springboot.exception;

/**
 * @Author : fuzhong
 * @CreateTime : 2020/6/23 10:43
 * @Description : 自定义异常
 **/
public class CustomException extends RuntimeException {
    private String message;
    public CustomException () {
        super();
    }
    public CustomException (String message) {
        super(message);
        this.message = message;
    }
    @Override
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
}