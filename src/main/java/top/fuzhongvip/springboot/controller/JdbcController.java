package top.fuzhongvip.springboot.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.fuzhongvip.springboot.entity.Songzhan;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/jdbc")
@Api(value = "jdbc-test", tags = "jdbc-test")
public class JdbcController {
    @Value("${spring.datasource.url}")
    String url;
    @Value("${spring.datasource.driver-class-name}")
    String driveClassName;
    @Value("${spring.datasource.username}")
    String username;
    @Value("${spring.datasource.password}")
    String password;

    public Connection getConnection() {
        Connection conn = null;
        try {
            Class.forName(driveClassName);
            conn = DriverManager.getConnection(url, username, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }

    /**
     *
     * @description 分页查询数据
     * @param pageNum
     * @return
     */
    @RequestMapping(value = "/find", method = RequestMethod.GET)
    public List<Songzhan> find(@RequestParam(value = "pageNum") Integer pageNum,
                               @RequestParam(value = "pageSize") Integer pageSize) {
        List<Songzhan> list = new ArrayList<>();
        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "select * from songzhan where create_time = ? limit ?,?";
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, "2021-05-12 16:41:15");
            ps.setInt(2, (pageNum - 1) * pageSize);
            ps.setInt(3, pageSize);
            rs = ps.executeQuery();
            while (rs.next()) {
                Songzhan songzhan = new Songzhan();
                songzhan.setId(rs.getInt("id"));
                songzhan.setGroupId(rs.getString("group_id"));
                songzhan.setJsonParams(rs.getString("json_params"));
                songzhan.setIpAddr(rs.getString("ip_addr"));
                songzhan.setIpInfo(rs.getString("ip_info"));
                songzhan.setIsDelete(rs.getInt("is_delete"));

                Timestamp create_time = rs.getTimestamp("create_time");
                LocalDateTime createTime = create_time.toLocalDateTime();
                songzhan.setCreateTime(createTime);
                Timestamp update_time = rs.getTimestamp("update_time");
                LocalDateTime updateTime = update_time.toLocalDateTime();
                songzhan.setUpdateTime(updateTime);

                list.add(songzhan);
            }
            rs.close();
            ps.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.close(rs, ps, conn);
        }
        System.out.println(list);
        return list;
    }

    /**
     *
     * @description 查询总记录数
     * @return
     */
    @RequestMapping(value = "/findCount", method = RequestMethod.GET)
    public int findCount() {
        int count = 0;
        Connection conn = getConnection();
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "select count(1) from songzhan";
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                count = rs.getInt(1);
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.close(rs, stmt, conn);
        }
        System.out.println(count);
        return count;
    }

    private void close(ResultSet rs, Statement stmt, Connection conn){
        try {
            if(rs != null) {
                rs.close();
            }
            if(stmt != null) {
                stmt.close();
            }
            if(conn != null) {
                conn.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
