package top.fuzhongvip.springboot.controller;

import com.alibaba.fastjson.JSONArray;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.fuzhongvip.springboot.common.api.dto.FlightInfoIcreditDto;
import top.fuzhongvip.springboot.common.dto.FlightInfoVo;
import top.fuzhongvip.springboot.common.dto.JsonResp;
import top.fuzhongvip.springboot.utils.HttpUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author : fuzhong
 * @CreateTime : 2021/5/8 14:23
 * @Description : 航班信息查询 对接艾科瑞特iCREDIT接口
 **/
@RestController
@RequestMapping("/flight/icredit")
@Api(value = "航班信息查询接口", tags = "航班信息查询Api")
public class FlightIcreditController {

    private final static Logger log = LoggerFactory.getLogger(FlightIcreditController.class);

    @Value("${flightapi.icredit.alicloudapi.host}")
    private String host;
    @Value("${flightapi.icredit.alicloudapi.path}")
    private String path;
    @Value("${alicloudapi.method}")
    private String method;
    @Value("${alicloudapi.appcode}")
    private String appcode;

    /**
     * 获取飞机航班信息
     * @param date 日期, 如20200706
     * @param flightId 航班编号, 如HU7610
     * @return
     */
    @RequestMapping(value = "/getAirPlaneInfo", method = RequestMethod.GET)
    @ApiOperation(value = "获取飞机航班信息", notes = "获取飞机航班信息")
    public JsonResp getAirPlaneInfo(
            @ApiParam(required = true, value = "日期, 如20200706") @RequestParam(required = true) String date,
            @ApiParam(required = true, value = "航班编号, 如HU7610") @RequestParam(required = true) String flightId
    ){
        List<FlightInfoVo> resultList = new ArrayList<>();

        // header配置
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "APPCODE " + appcode);
        // 参数配置
        Map<String, String> querys = new HashMap<>();
        querys.put("DATE", date);//日期，如：20190208
        querys.put("FLIGHT_ID", flightId);//航班编号，如：MU5128

        try {
            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
            int statusCode = response.getStatusLine().getStatusCode(); //状态码
            if (200 == statusCode) {
                String entityString = EntityUtils.toString(response.getEntity());
                List<FlightInfoIcreditDto> flightInfoIcreditDtoList = JSONArray.parseArray(entityString, FlightInfoIcreditDto.class);
                if (flightInfoIcreditDtoList.size() == 1 && !"艾科瑞特，让企业业绩长青".equals(flightInfoIcreditDtoList.get(0).getFlightStatus())){
                    return JsonResp.failed(flightInfoIcreditDtoList.get(0).getFlightStatus());
                }
                flightInfoIcreditDtoList.stream().forEach(e -> {
                    FlightInfoVo vo = new FlightInfoVo();
                    vo.setFlightStatus(e.getFlightStatus());
                    vo.setFlightAirwaysCh(e.getFlightAirwaysCh()); //航空公司名称_中文
                    vo.setFlightId(e.getFlightId());//航班编号
                    vo.setStartTime(e.getStartTime());//出发时间
                    vo.setActualStartTime(e.getActualStartTime());//实际出发时间
                    vo.setStartAirportChStartTerminalEn(e.getStartAirportCh() + " " + e.getStartTerminalEn());//出发机场名称 航站楼
                    vo.setEndTime(e.getEndTime());//抵达时间
                    vo.setActualEndTime(e.getActualEndTime());//实际抵达时间
                    vo.setEndAirportChEndTerminalEn(e.getEndAirportCh() + " " + e.getEndTerminalEn());//抵达机场名称 航站楼
                    vo.setFlightActualStatus(e.getFlightActualStatus());//航班实际状态
                    resultList.add(vo);
                });
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return JsonResp.ok(resultList);
    }

    /**
     * 获取飞机航班信息 测试
     * @param date 日期, 如20200706
     * @param flightId 航班编号, 如HU7610
     * @return
     */
    @RequestMapping(value = "/getAirPlaneInfoTest", method = RequestMethod.GET)
    @ApiOperation(value = "获取飞机航班信息test", notes = "获取飞机航班信息")
    public JsonResp getAirPlaneInfoTest(
            @ApiParam(required = true, value = "日期, 如20200706") @RequestParam(required = true) String date,
            @ApiParam(required = true, value = "航班编号, 如HU7610") @RequestParam(required = true) String flightId
    ){
        List<FlightInfoVo> resultList = new ArrayList<>();
        FlightInfoVo vo1 = new FlightInfoVo();
        vo1.setFlightStatus("艾科瑞特，让企业业绩长青");
        vo1.setFlightAirwaysCh("南方航空");//航空公司名称_中文
        vo1.setFlightId("CZ9111");//航班编号
        vo1.setStartTime("06:50");//出发时间
        vo1.setActualStartTime("06:50");//实际出发时间
        vo1.setStartAirportChStartTerminalEn("西双版纳嘎洒机场");//出发机场名称-航站楼
        vo1.setEndTime("08:45");//抵达时间
        vo1.setActualEndTime("06:50");//实际抵达时间
        vo1.setEndAirportChEndTerminalEn("重庆江北国际机场 T2");//抵达机场名称-航站楼
        vo1.setFlightActualStatus("到达");//航班实际状态
        resultList.add(vo1);
        FlightInfoVo vo2 = new FlightInfoVo();
        vo2.setFlightStatus("艾科瑞特，让企业业绩长青");
        vo2.setFlightAirwaysCh("厦门航空");//航空公司名称_中文
        vo2.setFlightId("MF1119");//航班编号
        vo2.setStartTime("12:45");//出发时间
        vo2.setActualStartTime("12:45");//实际出发时间
        vo2.setStartAirportChStartTerminalEn("广州白云国际机场 T2");//出发机场名称-航站楼
        vo2.setEndTime("16:00");//抵达时间
        vo2.setActualEndTime("16:00");//实际抵达时间
        vo2.setEndAirportChEndTerminalEn("银川河东机场");//抵达机场名称-航站楼
        vo2.setFlightActualStatus("起飞");//航班实际状态
        resultList.add(vo2);
        FlightInfoVo vo3 = new FlightInfoVo();
        vo3.setFlightStatus("艾科瑞特，让企业业绩长青");
        vo3.setFlightAirwaysCh("四川航空");//航空公司名称_中文
        vo3.setFlightId("3U5111");//航班编号
        vo3.setStartTime("15:25");//出发时间
        vo3.setActualStartTime("15:25");//实际出发时间
        vo3.setStartAirportChStartTerminalEn("太原武宿机场 T2");//出发机场名称-航站楼
        vo3.setEndTime("18:15");//抵达时间
        vo3.setActualEndTime("18:15");//实际抵达时间
        vo3.setEndAirportChEndTerminalEn("昆明长水国际机场");//抵达机场名称-航站楼
        vo3.setFlightActualStatus("取消");//航班实际状态
        resultList.add(vo3);
        return JsonResp.ok(resultList);
    }
}
