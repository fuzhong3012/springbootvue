package top.fuzhongvip.springboot.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.redis.core.RedisTemplate;
import top.fuzhongvip.springboot.common.dto.JsonResp;
import top.fuzhongvip.springboot.config.RedissonConfig;
import top.fuzhongvip.springboot.entity.Songzhan;
import top.fuzhongvip.springboot.entity.VueDemoEntity;
import top.fuzhongvip.springboot.exception.CustomException;
import top.fuzhongvip.springboot.utils.IpUtils;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @Author : fuzhong
 * @CreateTime : 2021/4/25 10:05
 * @Description :
 **/
@RestController
@Api(value = "vueDemo接口", tags = "vueDemoApi")
public class VueDemoController {

    @Autowired
    private IpUtils ipUtils;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    RedissonConfig redissonConfig;

    @Value("${spring.redis.mode}")
    private String mode;

    @Autowired
    RedissonClient redissonClient;

    /**
     * redisson测试
     */
    @GetMapping("/redissonTest")
    @ApiOperation(value = "redissonTest")
    public JsonResp<String> redissonTest() {

        System.out.println(redissonConfig.getAddress());
        System.out.println(redissonConfig.getConnectionMinimumIdleSize());
        System.out.println(redissonConfig.getConnectionPoolSize());
        System.out.println(redissonConfig.getMasterConnectionPoolSize());
        System.out.println(redissonConfig.getMasterName());
        System.out.println(redissonConfig.getPassword());
        System.out.println(redissonConfig.getSchema());
        System.out.println(redissonConfig.getSentinelAddresses());
        System.out.println(redissonConfig.getSlaveConnectionPoolSize());
        System.out.println(redissonConfig.getTimeout());

        System.out.println(mode);


        RLock lock = redissonClient.getLock("123");
        System.out.println("获取锁");
        try{
            boolean b = lock.tryLock(5, 20, TimeUnit.SECONDS);
            System.out.println("尝试加锁");
            if(!b){
                throw new RuntimeException("-------------");
            }
            Thread.sleep(500L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            //lock.isLocked()：判断要解锁的key是否已被锁定。lock.isHeldByCurrentThread()：判断要解锁的key是否被当前线程持有。
            if (lock.isLocked() && lock.isHeldByCurrentThread()) {
                lock.unlock();
            }
            System.out.println("释放锁");
        }
        System.out.println("释放锁后执行。。。。。");
        return JsonResp.ok("success");
    }

    @GetMapping("/hello")
    @ApiOperation(value = "hello world", notes = "hello world 测试")
    public String hello(){
        return "hello world";
    }

    /**
     * 全局异常test1
     * @return
     */
    @GetMapping("/exceptionAll")
    @ApiOperation(value = "hello world", notes = "hello world 测试")
    public String exceptionAll(){
        try {
            int i = 1 / 0;
        } catch (Exception e){
            throw new CustomException("Exception转CustomException，测试一下全局异常");
        }
        return "hello world";
    }

    /**
     * 全局异常test2
     * @return
     */
    @GetMapping("/exceptionAll2")
    @ApiOperation(value = "hello world", notes = "hello world 测试")
    public String exceptionAll2(){
        if (true){
            throw new CustomException("测试一下全局异常");
        }
        return "hello world";
    }

    /**
     * redis测试
     * @return
     */
    @GetMapping("/redisTest")
    @ApiOperation(value = "hello world", notes = "hello world 测试")
    public String redisTest(){
        /*Object token = redisTemplate.opsForValue().get("token");
        System.out.println(token);*/
        Songzhan songzhan = new Songzhan();
        songzhan.setCreateTime(LocalDateTime.now());
        songzhan.setGroupId("aaaaaaa");
        songzhan.setId(11111);
        songzhan.setJsonParams("435eftre;gtfrjestiw4");
        String songzhanString = JSON.toJSONString(songzhan);
        System.out.println(songzhanString);
        // set
        redisTemplate.opsForValue().set("test:test1:test2", songzhanString, 1, TimeUnit.MINUTES);
        // get
        String s = redisTemplate.opsForValue().get("test:test1:test2");
        System.out.println(s);
        Songzhan songzhan1 = JSONObject.parseObject(s, Songzhan.class);
        String jsonParams = songzhan1.getJsonParams();
        System.out.println(jsonParams);
        // delete
        redisTemplate.delete("test:test1:test2");
        return s;
    }

    /**
     * redis自增测试
     * @return
     */
    @GetMapping("/increment")
    @ApiOperation(value = "redis increment", notes = "redis increment 测试")
    public String increment(){
        // 分布式id
        redisTemplate.opsForValue().increment("id:gen:test");
        // get
        String s = redisTemplate.opsForValue().get("id:gen:test");
        return s;
    }

    /**
     * 获取ip信息
     * @param ip
     * @return
     */
    @RequestMapping(value = "/getIpInfo", method = RequestMethod.GET)
    @ApiOperation(value = "获取ip信息", notes = "获取ip信息")
    public JsonResp getAirPlaneInfo(
            @ApiParam(required = true, value = "ip") @RequestParam(required = true) String ip
    ){
        return JsonResp.ok(ipUtils.getIpInfo(ip));
    }


    @GetMapping("/channelinfos")
    @ApiOperation(value = "为vueDemo返回数据", notes = "测试vue的表格功能")
    public JsonResp channelinfos(){
        List<VueDemoEntity> list = new ArrayList<>();

        VueDemoEntity v1 = new VueDemoEntity();
        v1.setShort_description_chinese("AA测试");
        v1.setTitle_chinese("aa测试");
        v1.setVisible("1测试");
        list.add(v1);

        VueDemoEntity v2 = new VueDemoEntity();
        v2.setShort_description_chinese("BB测试");
        v2.setTitle_chinese("bb测试");
        v2.setVisible("2测试");
        list.add(v2);

        VueDemoEntity v3 = new VueDemoEntity();
        v3.setShort_description_chinese("CC测试");
        v3.setTitle_chinese("cc测试");
        v3.setVisible("3测试");
        list.add(v3);

        return JsonResp.ok(list);
    }

    @GetMapping("/exceptionTest")
    @ApiOperation(value = "异常抛出", notes = "本接口测试自定义异常及抛出")
    public Map<String, Object> exceptionTest(
            @ApiParam(required = false, value = "name测试") @RequestParam(value = "name") String name
    ) {
        if (null == name) {
            throw new CustomException("抛出自定义异常");
        } else if ("" == name) {
            throw new RuntimeException("name is 空字符串");
        }

        System.out.println(name + "=========");
        Map<String, Object> resultMap = new HashMap<String, Object>();

        /**
         * 无论有无异常，finally都会执行，
         * return放在finally里面和外面都可以
         */
        try {
            int i = 1/0;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            resultMap.put("name", name);
        }

        return resultMap;
    }

}
