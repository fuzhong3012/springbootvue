package top.fuzhongvip.springboot.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import top.fuzhongvip.springboot.common.Constants;
import top.fuzhongvip.springboot.common.dto.*;
import top.fuzhongvip.springboot.service.SongzhanExcelService;
import top.fuzhongvip.springboot.service.FlightJisuDataExcelService;
import top.fuzhongvip.springboot.utils.ExcelUtils;
import top.fuzhongvip.springboot.utils.IpUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @Author : fuzhong
 * @CreateTime : 2021/4/25 10:46
 * @Description : 解析excel
 **/
@RestController
@RequestMapping("/excel")
@Api(value = "解析excel接口", tags = "解析excelApi")
public class ExcelController {
    private Logger logger = LoggerFactory.getLogger(ExcelController.class);

    @Autowired
    private SongzhanExcelService songzhanExcelService;

    @Autowired
    private FlightJisuDataExcelService flightJisuDataExcelService;

    /**
     * 航班信息-->解析上传的excel并入库
     * @param request
     * @param date
     * @param files
     * @return
     */
    @RequestMapping(value = "/flightImportExcel", method = RequestMethod.POST)
    @ApiOperation(value = "航班信息-->解析上传的excel并入库", notes = "航班信息-->解析上传的excel并入库")
    public JsonResp flightImportExcel(
            HttpServletRequest request,
            @ApiParam(required = false, value = "查询日期") @RequestParam(required = false) String date,
            @ApiParam(required = true, value = "上传的文件数") @RequestParam(required = true) MultipartFile... files
    ){
        // 默认查询明天
        if (StringUtils.isEmpty(date) || StringUtils.equals("null", date)){
            date = LocalDateTime.now().plusDays(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        }
        // 获取第一个文件
        MultipartFile file = files[0];
        // 解析excel文件
        List<SongAndFlightJisuDto> resultList = ExcelUtils.readExcel("", SongAndFlightJisuDto.class, file);
        // 调取极速接口, 获得航班详情
        List<FlightInfoVo> flightInfoVoList = flightJisuDataExcelService.getFlightJisuVoList(date, resultList);

        // 获取ip地址
        String ipAddr = IpUtils.getIpAddr(request);
        // 数据存库
        flightJisuDataExcelService.saveExcelInfo(ipAddr, date, flightInfoVoList, resultList);

        return JsonResp.ok(flightInfoVoList);
    }

    /**
     * 航班信息-->导出时读取最近一次的上传记录并生成excel
     * @param response
     * @param groupId
     * @param tempType
     */
    @RequestMapping(value = "/flightDownloadExcel", method = RequestMethod.GET)
    @ApiOperation(value = "航班信息-->导出时读取最近一次的上传记录并生成excel", notes = "航班信息-->导出时读取最近一次的上传记录并生成excel")
    public void flightDownloadExcel(
            HttpServletResponse response,
            @ApiParam(required = false, value = "本次上传分配的groupId") @RequestParam(required = false) String groupId,
            @ApiParam(required = true, value = "模板类型0生成文件1生成表头模板") @RequestParam(required = true) String tempType
    ){
        List<SongAndFlightJisuDto> resultList = new ArrayList<>();
        String fileName = "航班信息-temp.xlsx";
        if (StringUtils.equals("0", tempType)){ //正常生成文件
            // 获取最近一次的数据
            Map<String, Object> resultMap = flightJisuDataExcelService.jisuListByGroupId(groupId);
            String requestQueryDateParams = (String)resultMap.get("requestQueryDateParams");
            Long now = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();
            fileName = requestQueryDateParams + "航班信息" + now + ".xlsx"; // 拼接导出文件名
            resultList = (List<SongAndFlightJisuDto>)resultMap.get("resultList");
            List<SongAndFlightJisuOutputDto> resultList2 = new ArrayList<>();
            resultList.stream().forEach(e -> {
                SongAndFlightJisuOutputDto songAndFlightJisuOutputDto = new SongAndFlightJisuOutputDto();
                BeanUtils.copyProperties(e, songAndFlightJisuOutputDto);
                resultList2.add(songAndFlightJisuOutputDto);
            });
            ExcelUtils.writeExcel(response, fileName, resultList2, SongAndFlightJisuOutputDto.class);
            //ExcelUtils.writeExcel(fileName, resultList2, SongOutputDto.class);
        } else { // 表头模板
            ExcelUtils.writeExcel(response, fileName, resultList, SongAndFlightJisuDto.class);
            //ExcelUtils.writeExcel(fileName, resultList, SongAndFlightJisuDto.class);
        }
    }

    /**
     * 查询最近一次接口调用次数、当天调用次数和总调用次数
     * @param groupId
     * @return
     */
    @RequestMapping(value = "/getInvokeCountInfo", method = RequestMethod.GET)
    @ApiOperation(value = "查询最近一次接口调用次数、当天调用次数和总调用次数", notes = "查询最近一次接口调用次数、当天调用次数和总调用次数")
    public JsonResp getInvokeCountInfo(
            @ApiParam(required = false, value = "本次需要查询的groupId") @RequestParam(required = false) String groupId
    ){
        Map<String, Object> resultMap = flightJisuDataExcelService.getInvokeCountInfo(groupId);
        return JsonResp.ok(resultMap);
    }

    /**
     * 送站信息-->解析上传的excel并入库
     * @param request
     * @param files
     * @return
     */
    @RequestMapping(value = "/songzhanUploadExcel", method = RequestMethod.POST)
    @ApiOperation(value = "送站信息-->解析上传的excel并入库", notes = "送站信息-->解析上传的excel并入库")
    public JsonResp songzhanUploadExcel(
            HttpServletRequest request,
            @ApiParam(required = true, value = "上传的文件数组") @RequestParam(required = true) MultipartFile... files
    ){
        List<SongDto> resultList = new ArrayList<>();
        if (null != files) {
            for (MultipartFile file : files) {
                String masterWorker = ExcelUtils.getMasterWorker(file);
                List<SongDto> oneFileList = ExcelUtils.readExcel("", SongDto.class, file);
                oneFileList.stream().forEach(e -> e.setMasterWorker(masterWorker));
                // 获取送站List
                resultList.addAll(getResultOneFileList(oneFileList));
            }
        }

        String groupId = "";
        if (resultList.size() > 0){
            String ipAddr = IpUtils.getIpAddr(request);
            groupId = songzhanExcelService.saveExcel(ipAddr, resultList);
        } else {
            return JsonResp.failed("未查询到送站信息");
        }
        return JsonResp.ok(groupId);
    }

    /**
     * 按文件list获取该文件下的resultList
     * @param oneFileList
     * @return
     */
    private List<SongDto> getResultOneFileList(List<SongDto> oneFileList){
        List<SongDto> resultList = new ArrayList<>();
        boolean songzhan = false;
        for (int i = 0; i < oneFileList.size(); i++){
            SongDto songDto = oneFileList.get(i);
            if (StringUtils.equals("送站", songDto.getName())){
                songzhan = true;
                continue;
            }
            if (songzhan){
                resultList.add(songDto);
            }
        }
        return resultList;
    }

    /**
     * 送站信息-->读取最近一次的上传记录并生成excel
     * @param response
     * @param groupId
     * @param tempType 0生成文件1生成表头模板
     */
    @RequestMapping(value = "/songzhanDownloadExcel", method = RequestMethod.GET)
    @ApiOperation(value = "送站信息-->读取最近一次的上传记录并生成excel", notes = "送站信息-->读取最近一次的上传记录并生成excel")
    public void songzhanDownloadExcel(
            HttpServletResponse response,
            @ApiParam(required = true, value = "本次上传分配的groupId") @RequestParam(required = false) String groupId,
            @ApiParam(required = true, value = "模板类型0生成文件1生成表头模板") @RequestParam(required = true) String tempType
    ){
        List<SongDto> resultList = new ArrayList<>();
        String fileName = Constants.SONGZHAN_TEMP_FILENAME;
        if (StringUtils.equals("0", tempType)){ //正常生成文件
            fileName = Constants.SONGZHAN_FILENAME;
            resultList = songzhanExcelService.listByGroupId(groupId);
            List<SongOutputDto> resultList2 = new ArrayList<>();
            resultList.stream().forEach(e -> {
                SongOutputDto songOutputDto = new SongOutputDto();
                BeanUtils.copyProperties(e, songOutputDto);
                resultList2.add(songOutputDto);
            });
            ExcelUtils.writeExcel(response, fileName, resultList2, SongOutputDto.class);
            //ExcelUtils.writeExcel(fileName, resultList2, SongOutputDto.class);
        } else { //表头模板
            ExcelUtils.writeExcel(response, fileName, resultList, SongDto.class);
            //ExcelUtils.writeExcel(fileName, resultList, SongDto.class);
        }
    }

    /**
     * 通过时间和航班号查询航班信息
     * @param date
     * @param flightno
     * @return
     */
    @RequestMapping(value = "/getFlightInfo", method = RequestMethod.GET)
    public JsonResp getFlightInfo(
            @RequestParam(required = false) String date, @RequestParam(required = false) String flightno
    ){
        FlightInfoVo vo = flightJisuDataExcelService.getFlightInfoByDateAndFlightno(date, flightno);
        System.out.println(vo);
        return JsonResp.ok(vo);
    }

}
