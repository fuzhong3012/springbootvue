package top.fuzhongvip.springboot.utils;

import com.alibaba.fastjson.JSON;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import top.fuzhongvip.springboot.common.dto.IpInfoGaodeResultDto;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author : fuzhong
 * @CreateTime : 2021/4/27 16:25
 * @Description : 客户端IP地址
 **/
@Component
public class IpUtils {
    private final static Logger log = LoggerFactory.getLogger(IpUtils.class);

    @Value("${ipapi.gaode.alicloudapi.host}")
    private String host;
    @Value("${ipapi.gaode.alicloudapi.path}")
    private String path;
    @Value("${alicloudapi.method}")
    private String method;
    @Value("${alicloudapi.appcode}")
    private String appcode;

    /**
     * 调高德接口,将IP信息转换为地理位置信息
     * @param ip
     * @return IpInfoGaodeResultDto的status属性为null，表示为查询失败；status="1"表示查询成功
     */
    public IpInfoGaodeResultDto getIpInfo(String ip){
        IpInfoGaodeResultDto ipInfoGaodeResultDto = new IpInfoGaodeResultDto();
        // header配置
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "APPCODE " + appcode);
        // 参数配置
        Map<String, String> querys = new HashMap<>();
        querys.put("ip", ip);//ip

        try {
            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
            log.info("查询ip:" + ip + "返回的response -----> {}", response.toString());
            int statusCode = response.getStatusLine().getStatusCode(); //状态码
            if (200 == statusCode) {
                String entityString = EntityUtils.toString(response.getEntity());
                log.info("查询ip:" + ip + "返回的entity -----> {}", entityString);
                ipInfoGaodeResultDto = JSON.parseObject(entityString.replaceAll("\\[", "\"").replaceAll("\\]", "\""), IpInfoGaodeResultDto.class);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ipInfoGaodeResultDto;
    }

    /**
     * 获取客户端IP地址
     * @param request
     * @return
     */
    public static String getIpAddr(HttpServletRequest request) {
        String ipAddress = null;
        try {
            ipAddress = request.getHeader("x-forwarded-for");
            if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getHeader("Proxy-Client-IP");
            }
            if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getHeader("WL-Proxy-Client-IP");
            }
            if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getRemoteAddr();
                if (ipAddress.equals("127.0.0.1")) {
                    // 根据网卡取本机配置的IP
                    try {
                        ipAddress = InetAddress.getLocalHost().getHostAddress();
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                    }
                }
            }
            // 通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
            if (ipAddress != null) {
                if (ipAddress.contains(",")) {
                    return ipAddress.split(",")[0];
                } else {
                    return ipAddress;
                }
            } else {
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

}
