package top.fuzhongvip.springboot.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import top.fuzhongvip.springboot.entity.Songzhan;

import java.text.Collator;
import java.util.*;

/**
 * @Author : fuzhong
 * @CreateTime : 2021/4/26 20:44
 * @Description : 用来测试的方法
 **/
public class DemoMethod {

    public static void main(String[] args) {
        jsoupDemo1();

        /*String masterWorkerOld = "张三 11111111111111111操作人3333333";
        System.out.println(masterWorkerOld);
        String masterWorker = StringUtils.substringBefore(masterWorkerOld, "操作人").trim();
        System.out.println(masterWorker);*/
        /*getJsonObject();*/
        /*getChinaSort();*/
        /*String str = "  安翔 17710399629           操作人：赵18515515859  ";
        String string = StringUtils.substringBefore(str, "操作人").trim();
        System.out.println(string);*/
        /*Map<String, Object> resultMap = testException();
        System.out.println(resultMap.get("name"));*/
    }

    public static void jsoupDemo1(){
        String html = "<html><head><title>First parse</title></head>"
                + "<body><p>Parsed HTML into a doc.</p></body></html>";
        //该parse(String html, String baseUri)方法将输入HTML解析为新的Document
        Document doc = Jsoup.parse(html);
        System.out.println(doc);
        System.out.println(doc.body());
        System.out.println(doc.head());
        System.out.println("-----------------------------");
        Elements elements = doc.body().getElementsByTag("p");
        System.out.println(elements.get(0));
        System.out.println(elements.get(0).html());
    }

    /**
     * try--catch--finally异常
     * @return
     */
    public static Map<String, Object> testException(){
        Map<String, Object> resultMap = new HashMap<>();

        /**
         * 无论有无异常，finally都会执行，
         * return放在finally里面和外面都可以
         */
        try {
            //int i = 1/0;
            int i = 1/1;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            resultMap.put("name", "nameName异常");
        }

        return resultMap;
    }

    /**
     * JSONObject序列化
     */
    public static void getJsonObject(){
        Songzhan songzhan1 = new Songzhan();
        songzhan1.setId(1);
        songzhan1.setGroupId("32423");
        Songzhan songzhan2 = new Songzhan();
        songzhan2.setId(2);
        songzhan2.setGroupId("4767685");
        List<Songzhan> list = new ArrayList<>();
        list.add(songzhan1);
        list.add(songzhan2);

        String jsonStr = JSONObject.toJSONString(list);
        System.out.println(jsonStr);

        //List<Songzhan> songzhanList = JSONObject.parseObject(jsonStr, List.class);//json字符串直接转给java对象
        List<Songzhan> songzhanList = JSONArray.parseArray(jsonStr, Songzhan.class); //json字符串直接转为List<java>对象
        for (Songzhan e : songzhanList) {
            System.out.println(e.getId());
        }

    }

    /**
     * 文字按拼音排序
     */
    public static void getChinaSort(){
        /**
         * 文字字符串排序
         */
        String[] arr = { "刘刘", "李飞", "王五", "老三", "贝贝", "啊三" };
        Collator cmp = Collator.getInstance(java.util.Locale.CHINA);
        Arrays.sort(arr, cmp);
        List<String> list = Arrays.asList(arr);
        System.out.println(list);

        /**
         * 文字entity排序
         */
        List<Songzhan> students = new ArrayList<>();

        Songzhan b1 = new Songzhan();
        b1.setIpAddr("44");
        b1.setGroupId("刘刘");
        Songzhan b2 = new Songzhan();
        b2.setIpAddr("11");
        b2.setGroupId("李飞");
        Songzhan b3 = new Songzhan();
        b3.setIpAddr("66");
        b3.setGroupId("王五");
        Songzhan b4 = new Songzhan();
        b4.setIpAddr("33");
        b4.setGroupId("老三");

        students.add(b1);
        students.add(b2);
        students.add(b3);
        students.add(b4);

        Collator comparator = Collator.getInstance(Locale.CHINESE);
        Collections.sort(students, (o1, o2) -> comparator.compare(o1.getGroupId(), o2.getGroupId()));

        for (Songzhan stu : students) {
            System.out.println(stu.getGroupId());
        }
    }



}
