package top.fuzhongvip.springboot.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
/**
 * @Author : fuzhong
 * @CreateTime : 2021/4/26 14:35
 * @Description : 日期格式化工具类
 **/
public class DateFormatUtil {

    /**
     * excel日期格式为Date
     * @param str
     * @param pattern
     * @param locale
     * @return
     */
    public static Date parse(String str, String pattern, Locale locale) {
        if(str == null || pattern == null) {
            return null;
        }
        try {
            return new SimpleDateFormat(pattern, locale).parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String format(Date date, String pattern, Locale locale) {
        if(date == null || pattern == null) {
            return null;
        }
        return new SimpleDateFormat(pattern, locale).format(date);
    }

}
