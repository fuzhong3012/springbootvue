package top.fuzhongvip.springboot.utils;

import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Iterator;

/**
 * <p>
 * 文件处理工具
 */
@Slf4j
public class FileUtil {

    /**
     * 保存文件到指定目录
     *
     * @param fis
     * @param filePath
     * @param fileName
     * @throws
     */
    public static void uploadFile(InputStream fis, String filePath, String fileName) throws Exception {
        File file = new File(filePath);
        if (!file.exists()) {
            if (!file.mkdirs()) log.error("创建文件夹失败");
        }
        int i;
        byte[] bs = new byte[2 * 1024];
        String fullPath = file.getPath() + File.separator + fileName;
        OutputStream fos = new FileOutputStream(fullPath);
        while ((i = fis.read(bs)) > 0) {
            fos.write(bs, 0, i);
        }
        fis.close();
        fos.flush();
        fos.close();
    }


    /**
     * 下载文件
     *
     * @param response
     * @param path
     * @param fileName
     * @throws
     */
    public static void downloadFile(HttpServletResponse response, String path, String fileName) throws Exception {
        File file = new File(path + File.separator + fileName);
        if (file.exists() && file.isFile()) {
            response.setHeader("content-type", "application/octet-stream");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
            byte[] buff = new byte[1024];
            BufferedInputStream bis = null;
            OutputStream os;
            try {
                os = response.getOutputStream();
                bis = new BufferedInputStream(new FileInputStream(file));
                int i = bis.read(buff);
                while (i != -1) {
                    os.write(buff, 0, i);
                    os.flush();
                    i = bis.read(buff);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (bis != null) {
                    try {
                        bis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            throw new Exception("文件不存在");
        }
    }

    /**
     * 判断文件是否存在
     *
     * @param filePath 文件全路径
     * @return boolean
     */

    public static boolean fileExists(String filePath) {
        return new File(filePath).exists();
    }

    /**
     * 判断文件夹是否存在
     *
     * @param dirPath 文件夹全路径
     * @return boolean
     */
    public static boolean dirExists(String dirPath) {
        File file = new File(dirPath);
        return file.exists() && file.isDirectory();
    }

    /**
     * 删除文件
     *
     * @param filePath 文件全路径
     * @return Boolean
     */
    public static boolean deleteFile(String filePath) {
        Boolean status = Boolean.FALSE;
        try {
            File file = new File(filePath);
            if (file.exists() && !file.isDirectory()) {
                status = file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    /**
     * 拷贝文件
     *
     * @param oldFile 原始文件
     * @param newFile 新文件
     * @return 耗时
     */
    public static long copyFile(File oldFile, File newFile) throws Exception {
        long time = new Date().getTime();
        int length = 2097152;
        FileInputStream in = new FileInputStream(oldFile);
        if (!newFile.exists()) {
            newFile.getParentFile().mkdirs();
            newFile.createNewFile();
        }
        FileOutputStream out = new FileOutputStream(newFile);
        byte[] buffer = new byte[length];
        while (true) {
            int ins = in.read(buffer);
            if (ins == -1) {
                in.close();
                out.flush();
                out.close();
                return new Date().getTime() - time;
            } else {
                out.write(buffer, 0, ins);
            }
        }
    }

    /**
     * 递归删除目录下的所有文件及子目录下所有文件
     *
     * @param dir 将要删除的文件目录
     * @return {Boolean}
     */
    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            if (null != children) {
                for (String child : children) {
                    boolean success = deleteDir(new File(dir, child));
                    if (!success) {
                        return false;
                    }
                }
            }
        }
        return dir.delete();
    }

    /**
     * 向页面渲染图片
     *
     * @param picByte  byte[] 图片流
     * @param response HttpServletResponse
     */
    public static void renderImage(byte[] picByte, HttpServletResponse response) {
        ByteArrayInputStream fis = null;
        try {
            fis = new ByteArrayInputStream(picByte);
            ImageInputStream iis = ImageIO.createImageInputStream(fis);
            Iterator<ImageReader> iterator = ImageIO.getImageReaders(iis);
            ImageReader reader = iterator.next();
            String imgType = "image/" + reader.getFormatName().toLowerCase();
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0L);
            response.setContentType(imgType);
            OutputStream out = response.getOutputStream();
            byte[] buffer = new byte[1024];
            int len;
            fis = new ByteArrayInputStream(picByte);
            while ((len = fis.read(buffer)) != -1) {
                out.write(buffer, 0, len);
            }
            iis.close();
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 拷贝文件夹
     *
     * @param oldPath 旧文件夹路径
     * @param newPath 新文件件路径
     */
    public static void copyDir(String oldPath, String newPath) throws Exception {
        File file = new File(oldPath);
        if (file.exists() && file.isDirectory()) {
            File newFile = new File(newPath);
            String[] children = file.list();
            if (!newFile.exists()) {
                if (!newFile.mkdirs()) {
                    log.info("创建文件夹失败");
                }
            }
            if (children != null) {
                for (String c : children) {
                    String _oldPath = oldPath + File.separator + c;
                    String _newPath = newPath + File.separator + c;
                    File _oldFile = new File(_oldPath);
                    if (_oldFile.isDirectory()) {
                        copyDir(_oldPath, _newPath);
                    }
                    if (_oldFile.isFile()) {
                        copyFile(new File(_oldPath), new File(_newPath));
                    }
                }
            }
        }
    }

    /**
     * 将二进制流转换为文件
     *
     * @param path     文件路径
     * @param fileName 文件名，不包括后缀
     * @param suffix   文件后缀，包含点
     * @param data     二进制流
     */
    public static void byteToFile(byte[] data, String path, String fileName, String suffix) {
        BufferedOutputStream stream = null;
        FileOutputStream fstream = null;
        try {
            File filePath = new File(path);
            if (!filePath.exists() || !filePath.isDirectory()) {
                filePath.mkdirs();
            }
            File file = new File(path + "/" + fileName + suffix);
            if (!file.exists() || !file.isFile()) {
                file.createNewFile();
            }
            fstream = new FileOutputStream(file);
            stream = new BufferedOutputStream(fstream);
            stream.write(data);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stream != null) {
                    stream.close();
                }
                if (fstream != null) {
                    fstream.close();
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    /**
     * 创建目录 2020//4//16
     */
    public static String buildPath(String rootPath,LocalDateTime localDateTime){
        StringBuilder sb = new StringBuilder();
        int year = localDateTime.getYear();
        int monthValue = localDateTime.getMonthValue();
        int day = localDateTime.getDayOfMonth();
        return sb.append(rootPath).append(File.separator).
                append(year).append(File.separator).
                append(monthValue).append(File.separator).
                append(day).toString();
    }

}
