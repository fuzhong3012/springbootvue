package top.fuzhongvip.springboot.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author : fuzhong
 * @CreateTime : 2021/6/4 16:34
 * @Description : ipInfo接口返回数据result
 **/
@Data
public class IpInfoGaodeResultDto {

    @ApiModelProperty(value = "状态")
    private String status; //状态 1

    @ApiModelProperty(value = "info")
    private String info; //info OK

    @ApiModelProperty(value = "infocode")
    private String infocode; //infocode 10000

    @ApiModelProperty(value = "省")
    private String province; //省

    @ApiModelProperty(value = "市")
    private String city; //市

    @ApiModelProperty(value = "行政区划代码")
    private String adcode; //行政区划代码

    @ApiModelProperty(value = "坐标")
    private String rectangle; //坐标

}
