package top.fuzhongvip.springboot.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author : fuzhong
 * @CreateTime : 2021/5/8 22:46
 * @Description : 航班信息vo
 **/
@Data
public class FlightInfoVo {

    @ApiModelProperty(value = "航班接口查询状态")
    private String flightStatus; //航班接口查询状态

    @ApiModelProperty(value = "航空公司")
    private String flightAirwaysCh; //航空公司名称_中文

    @ApiModelProperty(value = "航班编号")
    private String flightId; //航班编号

    @ApiModelProperty(value = "计划起飞")
    private String startTime; //出发时间

    @ApiModelProperty(value = "实际起飞")
    private String actualStartTime; //实际出发时间

    @ApiModelProperty(value = "出发地")
    private String startAirportChStartTerminalEn; //出发城市 出发机场名称 航站楼

    @ApiModelProperty(value = "计划到达")
    private String endTime; //抵达时间

    @ApiModelProperty(value = "实际到达")
    private String actualEndTime; //实际抵达时间

    @ApiModelProperty(value = "到达地")
    private String endAirportChEndTerminalEn; //抵达城市 抵达机场名称 航站楼

    @ApiModelProperty(value = "状态")
    private String flightActualStatus; //航班实际状态

}
