package top.fuzhongvip.springboot.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created on 2017/11/6.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class JsonResp<T> {
    public static final String STATUS_SUCCESS = "success";
    public static final String STATUS_FAILURE = "failed";
    public static final String STATUS_ERROR = "error";
    public static final String STATUS_EXPIRE = "expire";
    public static final String STATUS_REMIND = "remind";
    public static final String STATUS_LOGOUT = "logout";
    public static final String ERROR_MSG = "接口异常，请联系工程师";

    private String status = STATUS_SUCCESS;

    private String message;

    private T data;

    public JsonResp(String message, T data) {
        this.message = message;
        this.data = data;
    }

    public static <T> JsonResp<T> ok(T data) {
        return restResult(STATUS_SUCCESS, "成功", data);
    }

    public static <T> JsonResp<T> ok() {
        return ok(null);
    }

    public static JsonResp failed() {
        return failed("失败");
    }

    public static JsonResp failed(String msg) {
        return failed(STATUS_FAILURE, msg);
    }

    public static JsonResp failed(String status, String msg) {
        return restResult(status, msg, null);
    }

    private static <T> JsonResp<T> restResult(String status, String message, T data) {
        JsonResp<T> apiResult = new JsonResp<>();
        apiResult.setStatus(status);
        apiResult.setMessage(message);
        apiResult.setData(data);
        return apiResult;
    }

}
