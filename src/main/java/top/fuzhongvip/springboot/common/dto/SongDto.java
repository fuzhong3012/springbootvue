package top.fuzhongvip.springboot.common.dto;

import lombok.Data;
import top.fuzhongvip.springboot.common.interfaces.ExcelColumn;

/**
 * @Author : fuzhong
 * @CreateTime : 2021/4/25 10:43
 * @Description :姓名 大人人数 小孩人数 手机号 酒店 车次 站台 时间 备注 导游或线路
 **/
@Data
public class SongDto {
    @ExcelColumn(value = "姓名", col = 1)
    private String name;

    @ExcelColumn(value = "大", col = 2)
    private String manCount;

    @ExcelColumn(value = "小", col = 3)
    private String childCount;

    @ExcelColumn(value = "手机号", col = 4)
    private String phone;

    @ExcelColumn(value = "酒店", col = 5)
    private String hotel;

    @ExcelColumn(value = "车次", col = 6)
    private String trainNumber;

    @ExcelColumn(value = "站台", col = 7)
    private String platform;

    @ExcelColumn(value = "时间", col = 8)
    private String leaveTime;

    @ExcelColumn(value = "备注", col = 9)
    private String remark;

    @ExcelColumn(value = "线路", col = 10)
    private String guide;

    @ExcelColumn(value = "师傅", col = 11)
    private String masterWorker;

    @Override
    public String toString() {
        return "SongDto [name=" + name + ", manCount=" + manCount + ", childCount=" + childCount + ", phone=" + phone
                + ", hotel=" + hotel + ", trainNumber=" + trainNumber + ", platform=" + platform + ", leaveTime=" + leaveTime
                + ", remark=" + remark + ", guide=" + guide + ", masterWorker=" + masterWorker + "]";
    }


}

