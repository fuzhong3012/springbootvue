package top.fuzhongvip.springboot.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.fuzhongvip.springboot.common.interfaces.ExcelColumn;

/**
 * @Author : fuzhong
 * @CreateTime : 2021/06/16 23:53
 * @Description :姓名 车次 出发地 起飞时间 到达地 落地时间 状态
 **/
@Data
public class SongAndFlightJisuOutputDto {

    @ExcelColumn(value = "姓名", col = 1)
    private String name;

    @ExcelColumn(value = "车次", col = 2)
    private String trainNumber;

    @ExcelColumn(value = "出发地", col = 3)
    @ApiModelProperty(value = "出发地")
    private String startAirportChStartTerminalEn; //出发机场名称-航站楼

    @ExcelColumn(value = "起飞时间", col = 4)
    @ApiModelProperty(value = "起飞时间")
    private String startTime; //出发时间/计划起飞

    @ExcelColumn(value = "到达地", col = 5)
    @ApiModelProperty(value = "到达地")
    private String endAirportChEndTerminalEn; //抵达机场名称-航站楼

    @ExcelColumn(value = "落地时间", col = 6)
    @ApiModelProperty(value = "落地时间")
    private String endTime; //抵达时间/计划到达

    @ExcelColumn(value = "状态", col = 7)
    @ApiModelProperty(value = "状态")
    private String flightActualStatus; //航班实际状态

    @Override
    public String toString() {
        return "SongDto [name=" + name + ", trainNumber=" + trainNumber + ", startAirportChStartTerminalEn=" + startAirportChStartTerminalEn
                + ", startTime=" + startTime + ", endAirportChEndTerminalEn=" + endAirportChEndTerminalEn
                + ", endTime=" + endTime + ", flightActualStatus=" + flightActualStatus + "]";
    }


}
