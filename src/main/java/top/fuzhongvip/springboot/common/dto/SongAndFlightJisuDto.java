package top.fuzhongvip.springboot.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.fuzhongvip.springboot.common.interfaces.ExcelColumn;

/**
 * @Author : fuzhong
 * @CreateTime : 2021/06/01 23:43
 * @Description :姓名 大人人数 小孩人数 手机号 酒店 车次 站台 时间 备注 导游或线路 师傅 组团社 出发地 起飞时间 到达地 落地时间 状态
 **/
@Data
public class SongAndFlightJisuDto {

    @ExcelColumn(value = "姓名", col = 1)
    private String name;

    @ExcelColumn(value = "大", col = 2)
    private String manCount;

    @ExcelColumn(value = "小", col = 3)
    private String childCount;

    @ExcelColumn(value = "手机号", col = 4)
    private String phone;

    @ExcelColumn(value = "酒店", col = 5)
    private String hotel;

    @ExcelColumn(value = "车次", col = 6)
    private String trainNumber;

    @ExcelColumn(value = "站台", col = 7)
    private String platform;

    @ExcelColumn(value = "时间", col = 8)
    private String leaveTime;

    @ExcelColumn(value = "备注", col = 9)
    private String remark;

    @ExcelColumn(value = "线路", col = 10)
    private String guide;

    @ExcelColumn(value = "师傅", col = 11)
    private String masterWorker;

    @ExcelColumn(value = "组团社", col = 12)
    @ApiModelProperty(value = "组团社")
    private String touristAgency; //组团社

    @ExcelColumn(value = "出发地", col = 13)
    @ApiModelProperty(value = "出发地")
    private String startAirportChStartTerminalEn; //出发机场名称-航站楼

    @ExcelColumn(value = "起飞时间", col = 14)
    @ApiModelProperty(value = "起飞时间")
    private String startTime; //出发时间/计划起飞

    @ExcelColumn(value = "到达地", col = 15)
    @ApiModelProperty(value = "到达地")
    private String endAirportChEndTerminalEn; //抵达机场名称-航站楼

    @ExcelColumn(value = "落地时间", col = 16)
    @ApiModelProperty(value = "落地时间")
    private String endTime; //抵达时间/计划到达

    @ExcelColumn(value = "状态", col = 17)
    @ApiModelProperty(value = "状态")
    private String flightActualStatus; //航班实际状态

    @Override
    public String toString() {
        return "SongDto [name=" + name + ", manCount=" + manCount + ", childCount=" + childCount + ", phone=" + phone
                + ", hotel=" + hotel + ", trainNumber=" + trainNumber + ", platform=" + platform + ", leaveTime=" + leaveTime
                + ", remark=" + remark + ", guide=" + guide + ", masterWorker=" + masterWorker
                + ", touristAgency=" + touristAgency + ", startAirportChStartTerminalEn=" + startAirportChStartTerminalEn + ", startTime=" + startTime
                + ", endAirportChEndTerminalEn=" + endAirportChEndTerminalEn
                + ", endTime=" + endTime + ", flightActualStatus=" + flightActualStatus + "]";
    }


}
