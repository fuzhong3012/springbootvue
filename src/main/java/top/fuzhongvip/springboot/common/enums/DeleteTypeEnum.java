package top.fuzhongvip.springboot.common.enums;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Author : fuzhong
 * @CreateTime : 2021/4/27 16:16
 * @Description : 删除标识枚举
 **/
public enum DeleteTypeEnum {

    /**
     * 否
     */
    NOT_DELETED("否", 0),
    /**
     * 是
     */
    DELETED("是", 1);

    private String name;
    private int index;

    private static Map<Integer, DeleteTypeEnum> collect;
    static {
        DeleteTypeEnum[] values = DeleteTypeEnum.values();
        collect = Arrays.stream(values).collect(Collectors.toMap(DeleteTypeEnum::getIndex, Function.identity()));
    }

    DeleteTypeEnum(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public static String getName(int index) {
        DeleteTypeEnum deleteTypeEnum = collect.get(index);
        if (deleteTypeEnum == null) {
            return null;
        }
        return deleteTypeEnum.getName();
    }
    // get set 方法
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getIndex() {
        return index;
    }
    public void setIndex(int index) {
        this.index = index;
    }

}

