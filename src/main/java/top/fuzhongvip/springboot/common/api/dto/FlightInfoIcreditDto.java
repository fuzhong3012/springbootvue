package top.fuzhongvip.springboot.common.api.dto;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author : fuzhong
 * @CreateTime : 2021/5/8 15:46
 * @Description : iCREDIT航班接口返回数据
 **/
@Data
public class FlightInfoIcreditDto {

    @ApiModelProperty(value = "航班接口查询状态")
    @JSONField(name = "FLIGHT_STATUS")
    private String flightStatus; //航班接口查询状态

    @ApiModelProperty(value = "航班实际状态")
    @JSONField(name = "FLIGHT_ACTUAL_STATUS")
    private String flightActualStatus; //航班实际状态

    @ApiModelProperty(value = "航班编号")
    @JSONField(name = "FLIGHT_ID")
    private String flightId; //航班编号

    @ApiModelProperty(value = "航空公司名称_中文")
    @JSONField(name = "FLIGHT_AIRWAYS_CH")
    private String flightAirwaysCh; //航空公司名称_中文

    @ApiModelProperty(value = "出发机场名称_中文")
    @JSONField(name = "START_AIRPORT_CH")
    private String startAirportCh; //出发机场名称_中文

    @ApiModelProperty(value = "出发机场名称_英文")
    @JSONField(name = "START_AIRPORT_EN")
    private String startAirportEn; //出发机场名称_英文

    @ApiModelProperty(value = "出发城市")
    @JSONField(name = "START_CITY")
    private String startCity; //出发城市

    @ApiModelProperty(value = "出发时间")
    @JSONField(name = "START_TIME")
    private String startTime; //出发时间

    @ApiModelProperty(value = "实际出发时间")
    @JSONField(name = "ACTUAL_START_TIME")
    private String actualStartTime; //实际出发时间

    @ApiModelProperty(value = "出发日期")
    @JSONField(name = "START_DATE")
    private String startDate; //出发日期

    @ApiModelProperty(value = "出发航站楼编码_英文")
    @JSONField(name = "START_TERMINAL_EN")
    private String startTerminalEn; //出发航站楼编码_英文

    @ApiModelProperty(value = "抵达机场名称_中文")
    @JSONField(name = "END_AIRPORT_CH")
    private String endAirportCh; //抵达机场名称_中文

    @ApiModelProperty(value = "抵达机场名称_英文")
    @JSONField(name = "END_AIRPORT_EN")
    private String endAirportEn; //抵达机场名称_英文

    @ApiModelProperty(value = "抵达城市")
    @JSONField(name = "END_CITY")
    private String endCity; //抵达城市

    @ApiModelProperty(value = "抵达时间")
    @JSONField(name = "END_TIME")
    private String endTime; //抵达时间

    @ApiModelProperty(value = "实际抵达时间")
    @JSONField(name = "ACTUAL_END_TIME")
    private String actualEndTime; //实际抵达时间

    @ApiModelProperty(value = "抵达日期")
    @JSONField(name = "END_DATE")
    private String endDate; //抵达日期

    @ApiModelProperty(value = "抵达航站楼编码_英文")
    @JSONField(name = "END_TERMINAL_EN")
    private String endTerminalEn; //抵达航站楼编码_英文

}
