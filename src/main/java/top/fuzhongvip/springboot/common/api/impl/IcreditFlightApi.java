package top.fuzhongvip.springboot.common.api.impl;

import com.alibaba.fastjson.JSONArray;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import top.fuzhongvip.springboot.common.api.FlightApi;
import top.fuzhongvip.springboot.common.api.dto.FlightInfoIcreditDto;
import top.fuzhongvip.springboot.common.dto.FlightInfoVo;
import top.fuzhongvip.springboot.utils.HttpUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

@Slf4j
@Component
@ConditionalOnProperty(prefix = "flight.api", value = "type", havingValue = "icredit")
public class IcreditFlightApi implements FlightApi {

    @Value("${flightapi.icredit.alicloudapi.host}")
    private String host;
    @Value("${flightapi.icredit.alicloudapi.path}")
    private String path;
    @Value("${alicloudapi.method}")
    private String method;
    @Value("${alicloudapi.appcode}")
    private String appcode;

    /**
     * 调icredit数据接口，获取飞机航班信息-多线程
     * @param date 日期, 如2021-06-07
     * @param flightno 航班编号, 如：MU2882
     * @param flightInfoVo
     * @param latch
     * @return
     */
    @Override
    @Async("taskExecutor")
    public void getAirPlaneInfoAsync(String date, String flightno, FlightInfoVo flightInfoVo, CountDownLatch latch) {
        log.info("航班编号:{} 子线程:{} -----> start", flightno, Thread.currentThread().getName());

        // header配置
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "APPCODE " + appcode);
        // 参数配置
        Map<String, String> querys = new HashMap<>();
        // 转换时间格式，去掉-
        querys.put("DATE", date.replace("-", ""));//日期，如：20190208
        querys.put("FLIGHT_ID", flightno);//航班编号，如：MU5128

        try {
            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
            log.info("航班编号:" + flightno + ",日期:" + date + "返回的response -----> {}", response.toString());
            int statusCode = response.getStatusLine().getStatusCode(); //状态码
            flightInfoVo.setFlightStatus(statusCode + "");//接口状态
            flightInfoVo.setFlightId(flightno);//先填上航班编号,如果查询到就覆盖这个字段
            if (200 == statusCode) {
                String entityString = EntityUtils.toString(response.getEntity());
                log.info("航班编号:" + flightno + ",日期:" + date + "返回的entity -----> {}", entityString);
                List<FlightInfoIcreditDto> list = JSONArray.parseArray(entityString, FlightInfoIcreditDto.class);
                if (CollectionUtils.isEmpty(list)){
                    return;
                }
                if (list.size() == 1 && !"艾科瑞特，让企业业绩长青".equals(list.get(0).getFlightStatus())){
                    flightInfoVo.setFlightActualStatus(list.get(0).getFlightStatus());
                    return;
                }
                if (list.size() == 1){
                    FlightInfoIcreditDto flightInfoIcreditDto = list.get(0);
                    buildFlightInfoVo(flightInfoVo, flightInfoIcreditDto);
                } else { // 大于1
                    FlightInfoIcreditDto flightInfoIcreditDto = list.get(0);
                    for (int i = 0; i < list.size(); i++) { // 含有"北京"就覆盖
                        if (list.get(i).getStartCity().contains("北京") || list.get(i).getEndCity().contains("北京")){
                            flightInfoIcreditDto = list.get(i);
                            break;
                        }
                    }
                    buildFlightInfoVo(flightInfoVo, flightInfoIcreditDto);
                    flightInfoVo.setFlightActualStatus(flightInfoVo.getFlightActualStatus() + "--经停");
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            latch.countDown();
        }
        log.info("航班编号:{} 子线程:{} -----> end", flightno, Thread.currentThread().getName());
    }

    /**
     * 调icredit数据接口，获取飞机航班信息-单线程
     * @param date 日期, 如2021-06-07
     * @param flightno 航班编号, 如：MU2882
     * @param flightInfoVo
     * @return
     */
    @Override
    public void getAirPlaneInfo(String date, String flightno, FlightInfoVo flightInfoVo) {
        // header配置
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "APPCODE " + appcode);
        // 参数配置
        Map<String, String> querys = new HashMap<>();
        // 转换时间格式，去掉-
        querys.put("DATE", date.replace("-", ""));//日期，如：20190208
        querys.put("FLIGHT_ID", flightno);//航班编号，如：MU5128

        try {
            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
            log.info("航班编号:" + flightno + ",日期:" + date + "返回的response -----> {}", response.toString());
            int statusCode = response.getStatusLine().getStatusCode(); //状态码
            flightInfoVo.setFlightStatus(statusCode + "");//接口状态
            flightInfoVo.setFlightId(flightno);//先填上航班编号,如果查询到就覆盖这个字段
            if (200 == statusCode) {
                String entityString = EntityUtils.toString(response.getEntity());
                log.info("航班编号:" + flightno + ",日期:" + date + "返回的entity -----> {}", entityString);
                List<FlightInfoIcreditDto> list = JSONArray.parseArray(entityString, FlightInfoIcreditDto.class);
                if (CollectionUtils.isEmpty(list)){
                    return;
                }
                if (list.size() == 1 && !"艾科瑞特，让企业业绩长青".equals(list.get(0).getFlightStatus())){
                    flightInfoVo.setFlightActualStatus(list.get(0).getFlightStatus());
                    return;
                }
                if (list.size() == 1){
                    FlightInfoIcreditDto flightInfoIcreditDto = list.get(0);
                    buildFlightInfoVo(flightInfoVo, flightInfoIcreditDto);
                } else { // 大于1
                    FlightInfoIcreditDto flightInfoIcreditDto = list.get(0);
                    for (int i = 0; i < list.size(); i++) { // 含有"北京"就覆盖
                        if (list.get(i).getStartCity().contains("北京") || list.get(i).getEndCity().contains("北京")){
                            flightInfoIcreditDto = list.get(i);
                            break;
                        }
                    }
                    buildFlightInfoVo(flightInfoVo, flightInfoIcreditDto);
                    flightInfoVo.setFlightActualStatus(flightInfoVo.getFlightActualStatus() + "--经停");
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 构造 flightInfoVo
     * @param flightInfoVo
     * @param flightInfoIcreditDto
     * 出发地/到达地格式化原则：北京首都国际机场只显示航站楼(T2/T3)，北京大兴国际机场只显示机场名(大兴机场)，其他显示城市机场名航站楼(青岛流亭国际机场T2)
     */
    private void buildFlightInfoVo(FlightInfoVo flightInfoVo, FlightInfoIcreditDto flightInfoIcreditDto){
        flightInfoVo.setFlightAirwaysCh(flightInfoIcreditDto.getFlightAirwaysCh());//航空公司名称_中文
        flightInfoVo.setFlightId(flightInfoIcreditDto.getFlightId());//航班编号
        //出发日期截取
        String depart = flightInfoIcreditDto.getStartTime();
        flightInfoVo.setStartTime(depart);//出发时间
        flightInfoVo.setActualStartTime(flightInfoIcreditDto.getActualStartTime());//实际出发时间
        if (StringUtils.equals("北京", flightInfoIcreditDto.getStartCity()) && StringUtils.contains(flightInfoIcreditDto.getStartAirportCh(), "首都")){
            flightInfoVo.setStartAirportChStartTerminalEn(flightInfoIcreditDto.getStartTerminalEn());//出发机场航站楼
        } else if(StringUtils.equals("北京", flightInfoIcreditDto.getStartCity()) && StringUtils.contains(flightInfoIcreditDto.getStartAirportCh(), "大兴")){
            flightInfoVo.setStartAirportChStartTerminalEn("大兴机场");//出发机场名称
        } else {
            flightInfoVo.setStartAirportChStartTerminalEn(flightInfoIcreditDto.getStartCity() + " " + flightInfoIcreditDto.getStartAirportCh() + " " + flightInfoIcreditDto.getStartTerminalEn());//出发城市 出发机场名称 航站楼
        }
        //抵达日期格式化
        String arrival = flightInfoIcreditDto.getEndTime();
        flightInfoVo.setEndTime(arrival);//抵达时间
        flightInfoVo.setActualEndTime(flightInfoIcreditDto.getActualEndTime());//实际抵达时间
        if (StringUtils.equals("北京", flightInfoIcreditDto.getEndCity()) && StringUtils.contains(flightInfoIcreditDto.getEndAirportCh(), "首都")){
            flightInfoVo.setEndAirportChEndTerminalEn(flightInfoIcreditDto.getEndTerminalEn());//抵达机场航站楼
        } else if(StringUtils.equals("北京", flightInfoIcreditDto.getEndCity()) && StringUtils.contains(flightInfoIcreditDto.getEndAirportCh(), "大兴")){
            flightInfoVo.setEndAirportChEndTerminalEn("大兴机场");//抵达机场名称
        } else {
            flightInfoVo.setEndAirportChEndTerminalEn(flightInfoIcreditDto.getEndCity() + " " + flightInfoIcreditDto.getEndAirportCh() + " " + flightInfoIcreditDto.getEndTerminalEn());//抵达城市 抵达机场名称 航站楼
        }
        flightInfoVo.setFlightActualStatus(flightInfoIcreditDto.getFlightActualStatus());//航班实际状态
    }

    @Override
    public void getpram(String date, String flightno, FlightInfoVo flightInfoVo, CountDownLatch latch) {
    }
}
