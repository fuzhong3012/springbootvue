package top.fuzhongvip.springboot.common.api.impl;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import top.fuzhongvip.springboot.common.api.FlightApi;
import top.fuzhongvip.springboot.common.api.dto.FlightInfoJisuDto;
import top.fuzhongvip.springboot.common.api.dto.FlightInfoJisuResultDto;
import top.fuzhongvip.springboot.common.dto.FlightInfoVo;
import top.fuzhongvip.springboot.utils.HttpUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

@Slf4j
@Component
@ConditionalOnProperty(prefix = "flight.api", value = "type", havingValue = "jisu")
public class JisuFlightApi implements FlightApi {

    @Value("${flightapi.jisu.alicloudapi.host}")
    private String host;
    @Value("${flightapi.jisu.alicloudapi.path}")
    private String path;
    @Value("${alicloudapi.method}")
    private String method;
    @Value("${alicloudapi.appcode}")
    private String appcode;

    /**
     * 调极速数据接口，获取飞机航班信息-多线程
     * @param date 日期, 如2021-06-07
     * @param flightno 航班编号, 如：MU2882
     * @param flightInfoVo
     * @param latch
     * @return
     */
    @Override
    @Async("taskExecutor")
    public void getAirPlaneInfoAsync(String date, String flightno, FlightInfoVo flightInfoVo, CountDownLatch latch){
        log.info("航班编号:{} 子线程:{} -----> start", flightno, Thread.currentThread().getName());

        // header配置
        Map<String, String> headers = new HashMap<>();
        // 最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        // 参数配置
        Map<String, String> querys = new HashMap<>();
        querys.put("date", date);//日期，如：2021-06-07
        querys.put("flightno", flightno);//航班编号，如：MU2882

        try {
            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
            log.info("航班编号:" + flightno + ",日期:" + date + "返回的response -----> {}", response.toString());
            int statusCode = response.getStatusLine().getStatusCode(); //状态码
            flightInfoVo.setFlightStatus(statusCode + "");//接口状态
            flightInfoVo.setFlightId(flightno);//先填上航班编号,如果查询到就覆盖这个字段
            if (200 == statusCode) {
                String entityString = EntityUtils.toString(response.getEntity());
                log.info("航班编号:" + flightno + ",日期:" + date + "返回的entity -----> {}", entityString);
                Map<String, String> map = JSON.parseObject(entityString, Map.class);
                if ("0".equals(JSON.toJSONString(map.get("status")))){
                    FlightInfoJisuResultDto flightInfoJisuResultDto = JSON.parseObject(JSON.toJSONString(map.get("result")), FlightInfoJisuResultDto.class);
                    List<FlightInfoJisuDto> list = flightInfoJisuResultDto.getList();
                    if (CollectionUtils.isEmpty(list)){
                        buildFlightInfoVo(flightInfoVo, flightInfoJisuResultDto);
                    } else if (list.size() == 1){
                        FlightInfoJisuDto flightInfoJisuDto = list.get(0);
                        buildFlightInfoVo(flightInfoVo, flightInfoJisuDto);
                    } else { // 大于1
                        FlightInfoJisuDto flightInfoJisuDto = list.get(0);
                        for (int i = 0; i < list.size(); i++) { // 含有"北京"就覆盖
                            if (list.get(i).getCity().contains("北京") || list.get(i).getEndcity().contains("北京")){
                                flightInfoJisuDto = list.get(i);
                                break;
                            }
                        }
                        buildFlightInfoVo(flightInfoVo, flightInfoJisuDto);
                        flightInfoVo.setFlightActualStatus(flightInfoVo.getFlightActualStatus() + "--经停");
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            latch.countDown();
        }
        log.info("航班编号:{} 子线程:{} -----> end", flightno, Thread.currentThread().getName());
    }

    /**
     * 调极速数据接口，获取飞机航班信息-单线程
     * @param date 日期, 如2021-06-07
     * @param flightno 航班编号, 如：MU2882
     * @param flightInfoVo
     * @return
     */
    @Override
    public void getAirPlaneInfo(String date, String flightno, FlightInfoVo flightInfoVo){
        // header配置
        Map<String, String> headers = new HashMap<>();
        // 最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        // 参数配置
        Map<String, String> querys = new HashMap<>();
        querys.put("date", date);//日期，如：2021-06-07
        querys.put("flightno", flightno);//航班编号，如：MU2882

        try {
            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
            log.info("航班编号:" + flightno + ",日期:" + date + "返回的response -----> {}", response.toString());
            int statusCode = response.getStatusLine().getStatusCode(); //状态码
            flightInfoVo.setFlightStatus(statusCode + "");//接口状态
            flightInfoVo.setFlightId(flightno);//先填上航班编号,如果查询到就覆盖这个字段
            if (200 == statusCode) {
                String entityString = EntityUtils.toString(response.getEntity());
                log.info("航班编号:" + flightno + ",日期:" + date + "返回的entity -----> {}", entityString);
                Map<String, String> map = JSON.parseObject(entityString, Map.class);
                if ("0".equals(JSON.toJSONString(map.get("status")))){
                    FlightInfoJisuResultDto flightInfoJisuResultDto = JSON.parseObject(JSON.toJSONString(map.get("result")), FlightInfoJisuResultDto.class);
                    List<FlightInfoJisuDto> list = flightInfoJisuResultDto.getList();
                    if (CollectionUtils.isEmpty(list)){
                        buildFlightInfoVo(flightInfoVo, flightInfoJisuResultDto);
                    } else if (list.size() == 1){
                        FlightInfoJisuDto flightInfoJisuDto = list.get(0);
                        buildFlightInfoVo(flightInfoVo, flightInfoJisuDto);
                    } else { // 大于1
                        FlightInfoJisuDto flightInfoJisuDto = list.get(0);
                        for (int i = 0; i < list.size(); i++) { // 含有"北京"就覆盖
                            if (list.get(i).getCity().contains("北京") || list.get(i).getEndcity().contains("北京")){
                                flightInfoJisuDto = list.get(i);
                                break;
                            }
                        }
                        buildFlightInfoVo(flightInfoVo, flightInfoJisuDto);
                        flightInfoVo.setFlightActualStatus(flightInfoVo.getFlightActualStatus() + "--经停");
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 构造 flightInfoVo
     * @param flightInfoVo
     * @param flightInfoJisuDto
     * 出发地/到达地格式化原则：北京首都国际机场只显示航站楼(T1/T2/T3)，北京大兴国际机场只显示机场名(大兴机场)，其他显示城市机场名航站楼(青岛流亭国际机场T2)
     */
    private void buildFlightInfoVo(FlightInfoVo flightInfoVo, FlightInfoJisuDto flightInfoJisuDto){
        flightInfoVo.setFlightAirwaysCh(flightInfoJisuDto.getAirline());//航空公司名称_中文
        flightInfoVo.setFlightId(flightInfoJisuDto.getFlightno());//航班编号
        //出发日期截取
        String depart = flightInfoJisuDto.getDepart();
        flightInfoVo.setStartTime(depart.substring(11, 16));//出发时间
        flightInfoVo.setActualStartTime(flightInfoJisuDto.getActualdepart());//实际出发时间
        if (StringUtils.equals("北京", flightInfoJisuDto.getCity()) && StringUtils.contains(flightInfoJisuDto.getDepartport(), "首都")){
            // 单独处理api接口返回的首都国际机场航站楼有些没有T的问题--首都国际机场有T1、T2、T3航站楼
            String departterminal = flightInfoJisuDto.getDepartterminal();
            if (StringUtils.equals("1", departterminal) || StringUtils.equals("2", departterminal) || StringUtils.equals("3", departterminal)){
                departterminal = "T" + departterminal;
            }
            flightInfoVo.setStartAirportChStartTerminalEn(departterminal);//出发机场航站楼
        } else if(StringUtils.equals("北京", flightInfoJisuDto.getCity()) && StringUtils.contains(flightInfoJisuDto.getDepartport(), "大兴")){
            flightInfoVo.setStartAirportChStartTerminalEn("大兴机场");//出发机场名称
        } else {
            flightInfoVo.setStartAirportChStartTerminalEn(flightInfoJisuDto.getCity() + " " + flightInfoJisuDto.getDepartport() + " " + flightInfoJisuDto.getDepartterminal());//出发城市 出发机场名称 航站楼
        }
        //抵达日期格式化
        String arrival = flightInfoJisuDto.getArrival();
        flightInfoVo.setEndTime(arrival.substring(11, 16));//抵达时间
        flightInfoVo.setActualEndTime(flightInfoJisuDto.getActualarrival());//实际抵达时间
        if (StringUtils.equals("北京", flightInfoJisuDto.getEndcity()) && StringUtils.contains(flightInfoJisuDto.getArrivalport(), "首都")){
            // 单独处理api接口返回的首都国际机场航站楼有些没有T的问题--首都国际机场有T1、T2、T3航站楼
            String arrivalterminal = flightInfoJisuDto.getArrivalterminal();
            if (StringUtils.equals("1", arrivalterminal) || StringUtils.equals("2", arrivalterminal) || StringUtils.equals("3", arrivalterminal)){
                arrivalterminal = "T" + arrivalterminal;
            }
            flightInfoVo.setEndAirportChEndTerminalEn(arrivalterminal);//抵达机场航站楼
        } else if(StringUtils.equals("北京", flightInfoJisuDto.getEndcity()) && StringUtils.contains(flightInfoJisuDto.getArrivalport(), "大兴")){
            flightInfoVo.setEndAirportChEndTerminalEn("大兴机场");//抵达机场名称
        } else {
            flightInfoVo.setEndAirportChEndTerminalEn(flightInfoJisuDto.getEndcity() + " " + flightInfoJisuDto.getArrivalport() + " " + flightInfoJisuDto.getArrivalterminal());//抵达城市 抵达机场名称 航站楼
        }
        flightInfoVo.setFlightActualStatus(flightInfoJisuDto.getStatus());//航班实际状态
    }

    /**
     * 假数据获取航班信息-测试用
     * @param date
     * @param flightno
     * @param flightInfoVo
     * @param latch
     * @return
     */
    @Override
    @Async("taskExecutor")
    public void getpram(String date, String flightno, FlightInfoVo flightInfoVo, CountDownLatch latch){
        log.info("航班编号:{} 子线程:{} -----> start", flightno, Thread.currentThread().getName());

        try {
            flightInfoVo.setFlightStatus("200");//接口状态
            flightInfoVo.setFlightId(flightno);//先填上航班编号,如果查询到就覆盖这个字段

            String body = "{\"status\":0,\"msg\":\"ok\",\"result\":{\"flightno\":\"CA8372\",\"date\":\"2021-06-07\",\"airline\":\"东方航空\",\"departport\":\"浦东国际机场\",\"city\":\"上海\",\"departportcode\":\"PVG\",\"arrivalport\":\"禄口国际机场\",\"endcity\":\"南京\",\"arrivalportcode\":\"NKG\",\"departterminal\":\"T1\",\"arrivalterminal\":\"T2\",\"depart\":\"2021-06-07 22:50:00\",\"arrival\":\"2021-06-08 00:10:00\",\"expecteddepart\":\"2021-06-07 22:50:00\",\"expectedarrival\":\"2021-06-08 00:10:00\",\"actualdepart\":\"\",\"actualarrival\":\"\",\"status\":\"计划\",\"arrpunctualrate\":\"\",\"list\":[{\"flightno\":\"CA8372\",\"airline\":\"东方航空\",\"departport\":\"浦东国际机场\",\"city\":\"上海\",\"departportcode\":\"PVG\",\"arrivalport\":\"禄口国际机场\",\"endcity\":\"南京\",\"arrivalportcode\":\"NKG\",\"departterminal\":\"T1\",\"arrivalterminal\":\"T2\",\"depart\":\"2021-06-07 22:50:00\",\"arrival\":\"2021-06-08 00:10:00\",\"expecteddepart\":\"2021-06-07 22:50:00\",\"expectedarrival\":\"2021-06-08 00:10:00\",\"actualdepart\":\"\",\"actualarrival\":\"\",\"status\":\"计划\",\"arrpunctualrate\":\"\"}]}}";
            //String body = "{\"status\":\"210\",\"msg\":\"未知错误\",\"result\":\"\"}";
            Map<String, Object> map = JSON.parseObject(body, Map.class);
            if ("0".equals(JSON.toJSONString(map.get("status")))) {
                FlightInfoJisuResultDto flightInfoJisuResultDto = JSON.parseObject(JSON.toJSONString(map.get("result")), FlightInfoJisuResultDto.class);
                List<FlightInfoJisuDto> list = flightInfoJisuResultDto.getList();
                if (CollectionUtils.isEmpty(list)) {
                    buildFlightInfoVo(flightInfoVo, flightInfoJisuResultDto);
                } else if (list.size() == 1) {
                    FlightInfoJisuDto flightInfoJisuDto = list.get(0);
                    buildFlightInfoVo(flightInfoVo, flightInfoJisuDto);
                } else { // 大于1
                    FlightInfoJisuDto flightInfoJisuDto = list.get(0);
                    for (int i = 0; i < list.size(); i++) { // 含有"北京"就覆盖
                        if (list.get(i).getCity().contains("北京") || list.get(i).getEndcity().contains("北京")) {
                            flightInfoJisuDto = list.get(i);
                            break;
                        }
                    }
                    buildFlightInfoVo(flightInfoVo, flightInfoJisuDto);
                    flightInfoVo.setFlightActualStatus(flightInfoVo.getFlightActualStatus() + "--经停");
                }
            }
            Thread.sleep(1000);
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            latch.countDown();
        }
        log.info("航班编号:{} 子线程:{} -----> end", flightno, Thread.currentThread().getName());
    }
}
