package top.fuzhongvip.springboot.common.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @Author : fuzhong
 * @CreateTime : 2021/5/30 00:16
 * @Description : 极速航班接口返回数据result
 */
@Data
@ToString(callSuper = true)
public class FlightInfoJisuResultDto extends FlightInfoJisuDto{

    @ApiModelProperty(value = "日期")
    private String date; //航班号

    @ApiModelProperty(value = "航班信息")
    private List<FlightInfoJisuDto> list; //航班信息
}
