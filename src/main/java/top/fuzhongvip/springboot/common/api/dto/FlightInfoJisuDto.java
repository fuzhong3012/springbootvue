package top.fuzhongvip.springboot.common.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author : fuzhong
 * @CreateTime : 2021/5/30 00:16
 * @Description : 极速航班接口返回数据
 */
@Data
public class FlightInfoJisuDto {

    @ApiModelProperty(value = "航班号")
    private String flightno; //航班号

    @ApiModelProperty(value = "航空公司")
    private String airline; //航空公司

    @ApiModelProperty(value = "出发机场")
    private String departport; //出发机场

    @ApiModelProperty(value = "出发城市")
    private String city; //出发城市

    @ApiModelProperty(value = "出发机场代号")
    private String departportcode; //出发机场代号

    @ApiModelProperty(value = "到达机场")
    private String arrivalport; //到达机场

    @ApiModelProperty(value = "到达城市")
    private String endcity; //到达城市

    @ApiModelProperty(value = "到达机场代号")
    private String arrivalportcode; //到达机场代号

    @ApiModelProperty(value = "出发机场航站楼")
    private String departterminal; //出发机场航站楼

    @ApiModelProperty(value = "到达机场航站楼")
    private String arrivalterminal; //到达机场航站楼

    @ApiModelProperty(value = "计划起飞时间")
    private String depart; //计划起飞时间

    @ApiModelProperty(value = "计划到达时间")
    private String arrival; //计划到达时间

    @ApiModelProperty(value = "预计起飞时间")
    private String expecteddepart; //预计起飞时间

    @ApiModelProperty(value = "预计到达时间")
    private String expectedarrival; //预计到达时间

    @ApiModelProperty(value = "实际起飞时间")
    private String actualdepart; //实际起飞时间

    @ApiModelProperty(value = "实际到达时间")
    private String actualarrival; //实际到达时间

    @ApiModelProperty(value = "航班状态")
    private String status; //航班状态

    @ApiModelProperty(value = "到达准点率")
    private String arrpunctualrate; //到达准点率

}
