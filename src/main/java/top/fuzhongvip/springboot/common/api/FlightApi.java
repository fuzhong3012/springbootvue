package top.fuzhongvip.springboot.common.api;

import top.fuzhongvip.springboot.common.dto.FlightInfoVo;

import java.util.concurrent.CountDownLatch;

public interface FlightApi {

    /**
     * 根据日期和航班号获取航班信息-多线程
     * @param date
     * @param flightno
     * @param flightInfoVo
     * @param latch
     * @return
     */
    void getAirPlaneInfoAsync(String date, String flightno, FlightInfoVo flightInfoVo, CountDownLatch latch);

    /**
     * 根据日期和航班号获取航班信息-单线程
     * @param date
     * @param flightno
     * @param flightInfoVo
     * @return
     */
    void getAirPlaneInfo(String date, String flightno, FlightInfoVo flightInfoVo);

    /**
     * 假数据获取航班信息-测试用
     * @param date
     * @param flightno
     * @param flightInfoVo
     * @param latch
     * @return
     */
    void getpram(String date, String flightno, FlightInfoVo flightInfoVo, CountDownLatch latch);

}
