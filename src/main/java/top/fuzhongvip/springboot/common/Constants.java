package top.fuzhongvip.springboot.common;

/**
 * @Author : fuzhong
 * @CreateTime : 2021/4/27 15:53
 * @Description : 常量
 **/
public class Constants {

    /**
     * songzhan excel模板文件名
     */
    public static final String SONGZHAN_TEMP_FILENAME = "songzhan-temp.xlsx";
    /**
     * songzhan excel文件名
     */
    public static final String SONGZHAN_FILENAME = "songzhan.xlsx";

    /**
     * excel默认的日期格式
     */
    public static final String EXCEL_DATETIME_FORMAT = "EEE MMM dd HH:mm:ss zzz yyyy";
    /**
     * 时间格式化1
     */
    public static final String TIME_FORMAT1 = "HH:mm";

    /**
     * 分隔符1
     */
    public static final String REGEX1 = "-";
}
