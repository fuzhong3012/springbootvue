package top.fuzhongvip.springboot.service;

import top.fuzhongvip.springboot.common.dto.FlightInfoVo;
import top.fuzhongvip.springboot.common.dto.SongAndFlightJisuDto;

import java.util.List;
import java.util.Map;

/**
 * @Author : fuzhong
 * @CreateTime : 2021/5/31 9:44
 * @Description :
 **/
public interface FlightJisuDataExcelService {

    /**
     * 导入时,通过date和fliaghts获取对应的航班信息
     * @param date
     * @param resultList
     * @return
     */
    List<FlightInfoVo> getFlightJisuVoList(String date, List<SongAndFlightJisuDto> resultList);

    /**
     * 导入时,保存导入的所有数据
     * @param ipAddr
     * @param date
     * @param flightInfoVoList
     * @param resultList
     * @return
     */
    String saveExcelInfo(String ipAddr, String date, List<FlightInfoVo> flightInfoVoList, List<SongAndFlightJisuDto> resultList);

    /**
     * 导出时,查询数据库中的航班信息
     * @param groupId
     * @return
     */
    Map<String, Object> jisuListByGroupId(String groupId);

    /**
     * 查询最近一次接口调用次数、当天调用次数和总调用次数
     * @param groupId
     * @return
     */
    Map<String, Object> getInvokeCountInfo(String groupId);

    /**
     * 通过时间和航班号查询航班信息
     * @param date
     * @param flightno
     * @return
     */
    FlightInfoVo getFlightInfoByDateAndFlightno(String date, String flightno);
}
