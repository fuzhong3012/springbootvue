package top.fuzhongvip.springboot.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.fuzhongvip.springboot.common.Constants;
import top.fuzhongvip.springboot.common.dto.IpInfoGaodeResultDto;
import top.fuzhongvip.springboot.common.dto.SongDto;
import top.fuzhongvip.springboot.common.enums.DeleteTypeEnum;
import top.fuzhongvip.springboot.entity.Songzhan;
import top.fuzhongvip.springboot.exception.CustomException;
import top.fuzhongvip.springboot.service.SongzhanExcelService;
import top.fuzhongvip.springboot.service.ISongzhanService;
import top.fuzhongvip.springboot.utils.DateFormatUtil;
import top.fuzhongvip.springboot.utils.IpUtils;

import java.text.Collator;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @Author : fuzhong
 * @CreateTime : 2021/4/26 20:46
 * @Description : 解析excel
 **/
@Service
public class SongzhanExcelServiceImpl implements SongzhanExcelService {

    @Autowired
    private ISongzhanService songzhanService;

    @Autowired
    private IpUtils ipUtils;

    @Override
    @Transactional
    public String saveExcel(String ipAddr, List<SongDto> resultList) {

        resultList.stream().forEach(e -> {
            //excel日期格式化
            String date = e.getLeaveTime();
            String format = date;
            if (StringUtils.isNotEmpty(date) && date.contains("1899")){ //如果是：Sun Dec 31 06:55:00 CST 1899
                Date parse = DateFormatUtil.parse(date, Constants.EXCEL_DATETIME_FORMAT, Locale.US);
                format = new SimpleDateFormat(Constants.TIME_FORMAT1).format(parse);
            }
            e.setLeaveTime(format);
            //处理“导游”为null
            if (StringUtils.isEmpty(e.getGuide())){
                e.setGuide("a");
            }
        });

        //按首字母排序
        Collator comparator = Collator.getInstance(Locale.CHINESE);
        Collections.sort(resultList, (o1, o2) -> comparator.compare(o1.getGuide(), o2.getGuide()));

        resultList.stream().forEach(e -> {
            //处理“导游”为null
            if (StringUtils.equals("a", e.getGuide())){
                e.setGuide(null);
            }
        });

        //全部删除
        songzhanService.update(new UpdateWrapper<Songzhan>().lambda().set(Songzhan::getIsDelete, DeleteTypeEnum.DELETED.getIndex()));

        //重新插入
        Songzhan songzhan = new Songzhan();
        String groupId = UUID.randomUUID().toString().replaceAll(Constants.REGEX1, "");
        LocalDateTime now = LocalDateTime.now();
        songzhan.setGroupId(groupId);
        songzhan.setJsonParams(JSONObject.toJSONString(resultList));
        songzhan.setIpAddr(ipAddr);
        IpInfoGaodeResultDto ipInfo = ipUtils.getIpInfo(ipAddr);
        songzhan.setIpInfo(ipInfo.getStatus() == null ? "" : ipInfo.getProvince() + " " + ipInfo.getCity());
        songzhan.setIsDelete(DeleteTypeEnum.NOT_DELETED.getIndex());
        songzhan.setCreateTime(now);
        songzhan.setUpdateTime(now);
        songzhanService.save(songzhan);

        return groupId;
    }

    @Override
    public List<SongDto> listByGroupId(String groupId) {

        List<Songzhan> list = songzhanService.list(new QueryWrapper<Songzhan>().lambda()
            .eq(StringUtils.isNotEmpty(groupId), Songzhan::getGroupId, groupId)
            .eq(StringUtils.isEmpty(groupId), Songzhan::getIsDelete, DeleteTypeEnum.NOT_DELETED.getIndex()));

        if (CollectionUtils.isEmpty(list)){
            return new ArrayList<>();
        }

        if (list.size() > 1){
            throw new CustomException("数据存在多行");
        }

        Songzhan songzhan = list.get(0);
        List<SongDto> resultList = JSONArray.parseArray(songzhan.getJsonParams(), SongDto.class); //json字符串直接转为List<java>对象

        return resultList;

    }

}
