package top.fuzhongvip.springboot.service.impl;

import top.fuzhongvip.springboot.entity.FlightJisuData;
import top.fuzhongvip.springboot.mapper.FlightJisuDataMapper;
import top.fuzhongvip.springboot.service.IFlightJisuDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fuzhong
 * @since 2021-05-31
 */
@Service
public class FlightJisuDataServiceImpl extends ServiceImpl<FlightJisuDataMapper, FlightJisuData> implements IFlightJisuDataService {

}
