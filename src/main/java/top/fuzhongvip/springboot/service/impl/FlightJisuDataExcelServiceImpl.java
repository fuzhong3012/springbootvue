package top.fuzhongvip.springboot.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.fuzhongvip.springboot.common.Constants;
import top.fuzhongvip.springboot.common.api.FlightApi;
import top.fuzhongvip.springboot.common.dto.*;
import top.fuzhongvip.springboot.common.enums.DeleteTypeEnum;
import top.fuzhongvip.springboot.entity.FlightJisuData;
import top.fuzhongvip.springboot.exception.CustomException;
import top.fuzhongvip.springboot.service.FlightJisuDataExcelService;
import top.fuzhongvip.springboot.service.IFlightJisuDataService;
import top.fuzhongvip.springboot.utils.DateFormatUtil;
import top.fuzhongvip.springboot.utils.IpUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Author : fuzhong
 * @CreateTime : 2021/5/31 9:44
 * @Description :
 **/
@Service
public class FlightJisuDataExcelServiceImpl implements FlightJisuDataExcelService {
    private final static Logger log = LoggerFactory.getLogger(FlightJisuDataExcelServiceImpl.class);

    @Autowired
    private IFlightJisuDataService flightJisuDataService;
    @Autowired
    private IpUtils ipUtils;
    @Resource
    private FlightApi flightApi;

    /**
     * 导入时,通过date和fliaghts获取对应的航班信息
     * @param date
     * @param resultList
     * @return
     */
    @Override
    public List<FlightInfoVo> getFlightJisuVoList(String date, List<SongAndFlightJisuDto> resultList) {
        List<FlightInfoVo> flightInfoVoList = new ArrayList<>();
        //获取flightnos
        List<String> flightnoList = resultList.stream()
                .filter(e -> StringUtils.isNotEmpty(e.getTrainNumber()) && e.getTrainNumber().trim().length() == 6)
                .map(SongAndFlightJisuDto::getTrainNumber)
                .collect(Collectors.toList());

        for (String flightno : flightnoList) {
            // 获取异步方法中处理后的数据结果，直接通过传参进行接收
            FlightInfoVo flightInfoVo = new FlightInfoVo();
            flightApi.getAirPlaneInfo(date, flightno, flightInfoVo);
            //flightApi.getpram(date, flightno, flightInfoVo, latch);
            flightInfoVoList.add(flightInfoVo);
        }

        /*CountDownLatch latch = new CountDownLatch(flightnoList.size());
        for (String flightno : flightnoList) {
            // 获取异步方法中处理后的数据结果，直接通过传参进行接收
            FlightInfoVo flightInfoVo = new FlightInfoVo();
            flightApi.getAirPlaneInfoAsync(date, flightno, flightInfoVo, latch);
            //flightApi.getpram(date, flightno, flightInfoVo, latch);
            flightInfoVoList.add(flightInfoVo);
        }
        log.info("主线程:{} -----------> 等待子线程执行 -----------", Thread.currentThread().getName());
        try {
            latch.await();
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
        }
        // 2023-06-06 由于极速的接口不支持高并发，多线程查询有可能会返回404，因此筛选出未查询到的航班，利用单线程再查一次
        for (FlightInfoVo flightInfoVo : flightInfoVoList) {
            if (StringUtils.equals(flightInfoVo.getFlightStatus(), "404")){
                flightApi.getAirPlaneInfo(date, flightInfoVo.getFlightId(), flightInfoVo);
            }
        }*/
        return flightInfoVoList;
    }

    /**
     * 导入时,保存导入的所有数据
     * @param ipAddr
     * @param date
     * @param flightInfoVoList
     * @param resultList
     * @return
     */
    @Override
    @Transactional
    public String saveExcelInfo(String ipAddr, String date, List<FlightInfoVo> flightInfoVoList, List<SongAndFlightJisuDto> resultList) {
        Map<String, FlightInfoVo> flightInfoVoMap = flightInfoVoList.stream().collect(Collectors.toMap(FlightInfoVo::getFlightId, Function.identity(), (value1, value2) -> value2));

        resultList.stream().forEach(e -> {
            //excel日期格式化
            String leaveTime = e.getLeaveTime();
            String format = leaveTime;
            if (StringUtils.isNotEmpty(leaveTime) && leaveTime.contains("1899")){ //如果是：Sun Dec 31 06:55:00 CST 1899
                Date parse = DateFormatUtil.parse(leaveTime, Constants.EXCEL_DATETIME_FORMAT, Locale.US);
                format = new SimpleDateFormat(Constants.TIME_FORMAT1).format(parse);
            }
            e.setLeaveTime(format);
            //航班信息赋值
            String trainNumber = StringUtils.isEmpty(e.getTrainNumber()) ? "" : e.getTrainNumber().toUpperCase();
            FlightInfoVo flightInfoVo = flightInfoVoMap.get(trainNumber);
            if (flightInfoVo != null){
                e.setStartTime(flightInfoVo.getStartTime());//出发时间
                e.setStartAirportChStartTerminalEn(flightInfoVo.getStartAirportChStartTerminalEn());//出发机场名称 航站楼
                e.setEndTime(flightInfoVo.getEndTime());//抵达时间
                e.setEndAirportChEndTerminalEn(flightInfoVo.getEndAirportChEndTerminalEn());//抵达机场名称 航站楼
                e.setFlightActualStatus(flightInfoVo.getFlightActualStatus());//航班实际状态
            }
        });

        //获取本次调用次数
        long invokeCount = flightInfoVoList.stream().count();

        //全部删除
        flightJisuDataService.update(new UpdateWrapper<FlightJisuData>().lambda().set(FlightJisuData::getIsDelete, DeleteTypeEnum.DELETED.getIndex()));

        //重新插入
        FlightJisuData flightJisuData = new FlightJisuData();
        String groupId = UUID.randomUUID().toString().replaceAll(Constants.REGEX1, "");
        LocalDateTime now = LocalDateTime.now();
        flightJisuData.setGroupId(groupId);
        flightJisuData.setRequestQueryDateParams(date);
        flightJisuData.setFlightInfoVoJsonParams(JSONObject.toJSONString(flightInfoVoList));
        flightJisuData.setSongAndFlightJisuDtoJsonParams(JSONObject.toJSONString(resultList));
        flightJisuData.setIpAddr(ipAddr);
        IpInfoGaodeResultDto ipInfo = ipUtils.getIpInfo(ipAddr);
        flightJisuData.setIpInfo(ipInfo.getStatus() == null ? "" : ipInfo.getProvince() + " " + ipInfo.getCity());
        flightJisuData.setInvokeCount((int)invokeCount);
        flightJisuData.setIsDelete(DeleteTypeEnum.NOT_DELETED.getIndex());
        flightJisuData.setCreateTime(now);
        flightJisuData.setUpdateTime(now);
        flightJisuDataService.save(flightJisuData);

        return groupId;
    }

    /**
     * 导出时,查询数据库中的航班信息
     * @param groupId
     * @return
     */
    @Override
    public Map<String, Object> jisuListByGroupId(String groupId) {
        Map<String, Object> resultMap = new HashMap<>();
        List<SongAndFlightJisuDto> resultList = new ArrayList<>();

        List<FlightJisuData> list = flightJisuDataService.list(new QueryWrapper<FlightJisuData>().lambda()
                .eq(StringUtils.isNotEmpty(groupId), FlightJisuData::getGroupId, groupId)
                .eq(StringUtils.isEmpty(groupId), FlightJisuData::getIsDelete, DeleteTypeEnum.NOT_DELETED.getIndex()));

        if (CollectionUtils.isEmpty(list)){
            resultMap.put("requestQueryDateParams", "");
            resultMap.put("resultList", resultList);
            return resultMap;
        }
        if (list.size() > 1){
            throw new CustomException("数据存在多行");
        }
        FlightJisuData flightJisuData = list.get(0);
        String requestQueryDateParams = flightJisuData.getRequestQueryDateParams();
        resultList = JSON.parseArray(flightJisuData.getSongAndFlightJisuDtoJsonParams(), SongAndFlightJisuDto.class); //json字符串直接转为List<java>对象

        resultMap.put("requestQueryDateParams", requestQueryDateParams);
        resultMap.put("resultList", resultList);
        return resultMap;
    }

    /**
     * 查询最近一次接口调用次数、当天调用次数和总调用次数
     * @param groupId
     * @return
     */
    @Override
    public Map<String, Object> getInvokeCountInfo(String groupId) {
        Map<String, Object> resultMap = new HashMap<>();

        //获取调用总次数
        List<FlightJisuData> countList = flightJisuDataService.list(new QueryWrapper<FlightJisuData>()
                .select("SUM(invoke_count) AS invokeCountAll"));
        FlightJisuData invokeCountData = CollectionUtils.isEmpty(countList) ? null : countList.get(0);
        int invokeCountAll = invokeCountData == null ? 0 : invokeCountData.getInvokeCountAll();
        resultMap.put("invokeCountAll", invokeCountAll);
        BigDecimal invokeCountAllRate = new BigDecimal(invokeCountAll + "").divide(new BigDecimal("10000"), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100"));
        resultMap.put("invokeCountAllRate", invokeCountAllRate.doubleValue());

        //获取今天调用次数
        LocalDate now = LocalDate.now();//代替calendar
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");//代替simpleDateFormat
        String nowStr = dtf.format(now);
        List<FlightJisuData> countTodayList = flightJisuDataService.list(new QueryWrapper<FlightJisuData>()
                .select("SUM(invoke_count) AS invokeCountAll")
                .lambda()
                .between(FlightJisuData::getCreateTime, nowStr + " 00:00:00", nowStr + " 23:59:59"));
        FlightJisuData invokeCountTodayData = CollectionUtils.isEmpty(countTodayList) ? null : countTodayList.get(0);
        int invokeCountToday = invokeCountTodayData == null ? 0 : invokeCountTodayData.getInvokeCountAll();
        resultMap.put("invokeCountToday", invokeCountToday);

        //获取本次调用次数
        List<FlightJisuData> list = flightJisuDataService.list(new QueryWrapper<FlightJisuData>().lambda()
                .eq(StringUtils.isNotEmpty(groupId), FlightJisuData::getGroupId, groupId)
                .eq(StringUtils.isEmpty(groupId), FlightJisuData::getIsDelete, DeleteTypeEnum.NOT_DELETED.getIndex()));
        if (CollectionUtils.isEmpty(list)){
            resultMap.put("invokeCount", 0);
            return resultMap;
        }
        if (list.size() > 1){
            throw new CustomException("数据存在多行");
        }
        FlightJisuData flightJisuData = list.get(0);
        int invokeCount = flightJisuData.getInvokeCount();

        resultMap.put("invokeCount", invokeCount);
        return resultMap;
    }

    /**
     * 通过时间和航班号查询航班信息
     * @param date
     * @param flightno
     * @return
     */
    @Override
    public FlightInfoVo getFlightInfoByDateAndFlightno(String date, String flightno) {
        FlightInfoVo flightInfoVo = new FlightInfoVo();
        flightApi.getAirPlaneInfo(date, flightno, flightInfoVo);
        return flightInfoVo;
    }

}
