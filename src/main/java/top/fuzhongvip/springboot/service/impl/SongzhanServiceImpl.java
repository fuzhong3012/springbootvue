package top.fuzhongvip.springboot.service.impl;

import top.fuzhongvip.springboot.entity.Songzhan;
import top.fuzhongvip.springboot.mapper.SongzhanMapper;
import top.fuzhongvip.springboot.service.ISongzhanService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fuzhong
 * @since 2021-04-26
 */
@Service
public class SongzhanServiceImpl extends ServiceImpl<SongzhanMapper, Songzhan> implements ISongzhanService {

}
