package top.fuzhongvip.springboot.service;

import top.fuzhongvip.springboot.entity.Songzhan;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuzhong
 * @since 2021-04-26
 */
public interface ISongzhanService extends IService<Songzhan> {

}
