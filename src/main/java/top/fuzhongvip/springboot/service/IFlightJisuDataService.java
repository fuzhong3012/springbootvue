package top.fuzhongvip.springboot.service;

import top.fuzhongvip.springboot.entity.FlightJisuData;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuzhong
 * @since 2021-05-31
 */
public interface IFlightJisuDataService extends IService<FlightJisuData> {

}
