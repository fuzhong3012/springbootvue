package top.fuzhongvip.springboot.service;

import top.fuzhongvip.springboot.common.dto.SongDto;

import java.util.List;

/**
 * @Author : fuzhong
 * @CreateTime : 2021/4/26 20:45
 * @Description : 解析excel
 **/
public interface SongzhanExcelService {

    /**
     * excel数据处理和入库
     * @param ipAddr 客户端IP地址
     * @param resultList
     * @return
     */
    String saveExcel(String ipAddr, List<SongDto> resultList);

    /**
     * 通过groupId查询未删除的数据
     * @param groupId
     */
    List<SongDto> listByGroupId(String groupId);
}
